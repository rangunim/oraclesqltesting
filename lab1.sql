--(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=ORACLE.win.ii.pwr.wroc.pl)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)SERVICE_NAME=orcl.win.ii.pwr.wroc.pl)))
ALTER SESSION SET NLS_Date_Format = 'YYYY-MM-DD';

Create Table Bandy(
nr_bandy number(2) constraint nr_bandy_pk PRIMARY KEY,
nazwa varchar2(20) NOT NULL,
teren varchar2(15) unique
);

Create table Funkcje(
funkcja varchar2(10) constraint funkcja_pk primary key,
min_myszy number(3) check (min_myszy >5),
max_myszy number(3),
check (max_myszy >= min_myszy And max_myszy < 200) -- wiecje niz 1 atrybut to tak
);

Create table Wrogowie(
imie_wroga varchar2(15) constraint imie_wroga_pk primary key,
stopien_wrogosci number(2) check (stopien_wrogosci>=1 and stopien_wrogosci <=10),
gatunek varchar2(15),
lapowka varchar2(20)
);

Create table Kocury(
imie varchar2(15) not null,
plec varchar2(1) check (plec='M' OR plec='K'),
pseudo varchar2(15) constraint pseudo_pk primary key,
funkcja varchar2(10) constraint funkcja_fk references Funkcje(funkcja) ,
szef varchar2(15) constraint szef_fk references Kocury(pseudo),
w_stadku_od date default sysdate,
przydzial_myszy number(3), 
myszy_ekstra number(3),
nr_bandy number(2) constraint nr_bandy_fk references Bandy(nr_bandy)
);

Create table Wrogowie_Kocurow(
pseudo varchar2(15) constraint pseudo_pk_fk references Kocury(pseudo),
imie_wroga constraint imie_Wroga_pk_fk references Wrogowie(imie_wroga),
data_incydentu date not null,
opis_incydentu varchar2(50)
);


INSERT INTO Bandy Values (1,'SZEFOSTWO','CALOSC');
INSERT INTO Bandy Values (2,'CZARNI RYCERZE','POLE');
INSERT INTO Bandy Values (3,'BIALI LOWCY','SAD');
INSERT INTO Bandy Values (4,'LACIACI MYSLIWI','GORKA');
INSERT INTO Bandy Values (5,'ROCKERSI','ZAGRODA');

INSERT INTO Funkcje Values ('SZEFUNIO',90,110);
INSERT INTO Funkcje Values ('BANDZIOR',70,90);
INSERT INTO Funkcje Values ('LOWCZY',60,70);
INSERT INTO Funkcje Values ('LAPACZ',50,60);
INSERT INTO Funkcje Values ('KOT',40,50);
INSERT INTO Funkcje Values ('MILUSIA',20,30);
INSERT INTO Funkcje Values ('DZIELCZY',45,55);
INSERT INTO Funkcje Values ('HONOROWA',6,25);

INSERT INTO Wrogowie Values ('KAZIO',10,'CZLOWIEK','FLASZKA');
INSERT INTO Wrogowie Values ('GLUPIA ZOSKA',1,'CZLOWIEK','KORALIK');
INSERT INTO Wrogowie Values ('SWAWOLNY DYZIO',7,'CZLOWIEK','GUMA DO ZUCIA');
INSERT INTO Wrogowie Values ('BUREK',4,'PIES','KOSC');
INSERT INTO Wrogowie Values ('DZIKI BILL',10,'PIES',NULL);
INSERT INTO Wrogowie Values ('REKSIO',2,'PIES','KOSC');
INSERT INTO Wrogowie Values ('BETHOVEN',1,'PIES','PEDIGRIPALL');
INSERT INTO Wrogowie Values ('CHYTRUSEK',5,'LIS','KURCZAK');
INSERT INTO Wrogowie Values ('SMUKLA',1,'SOSNA',NULL);
INSERT INTO Wrogowie Values ('BAZYLI',3,'KOGUT','KURA DO STADA');

INSERT INTO Wrogowie_Kocurow Values ('TYGRYS','KAZIO','2004-10-13','USILOWAL NABIC NA WIDLY');
INSERT INTO Wrogowie_Kocurow Values ('ZOMBI','SWAWOLNY DYZIO','2005-03-07','WYBIL OKO Z PROCY');
INSERT INTO Wrogowie_Kocurow Values ('BOLEK','KAZIO','2005-03-29','POSZCZUL BURKIEM');
INSERT INTO Wrogowie_Kocurow Values ('SZYBKA','GLUPIA ZOSKA','2006-09-12','UZYLA KOTA JAKO SCIERKI');
INSERT INTO Wrogowie_Kocurow Values ('MALA','CHYTRUSEK','2007-03-07','ZALECAL SIE');
INSERT INTO Wrogowie_Kocurow Values ('TYGRYS','DZIKI BILL','2007-06-12','USILOWAL POZBAWIC ZYCIA');
INSERT INTO Wrogowie_Kocurow Values ('BOLEK','DZIKI BILL','2007-11-10','ODGRYZL UCHO');
INSERT INTO Wrogowie_Kocurow Values ('LASKA','DZIKI BILL','2008-12-12','POGRYZL ZE LEDWO SIE WYLIZALA');
INSERT INTO Wrogowie_Kocurow Values ('LASKA','KAZIO','2009-01-07','ZLAPAL ZA OGON I ZROBIL WIATRAK');
INSERT INTO Wrogowie_Kocurow Values ('DAMA','KAZIO','2009-02-07','CHCIAL OBEDRZEC ZE SKORY');
INSERT INTO Wrogowie_Kocurow Values ('MAN','REKSIO','2009-04-14','WYJATKOWO NIEGRZECZNIE OBSZCZEKAL');
INSERT INTO Wrogowie_Kocurow Values ('LYSY','BETHOVEN','2009-05-11','NIE PODZIELIL SIE SWOJA KASZA');
INSERT INTO Wrogowie_Kocurow Values ('RURA','DZIKI BILL','2009-09-03','ODGRYZL OGON');
INSERT INTO Wrogowie_Kocurow Values ('PLACEK','BAZYLI','2010-07-12','DZIOBIAC UNIEMOZLIWIL PODEBRANIE KURCZAKA');
INSERT INTO Wrogowie_Kocurow Values ('PUSZYSTA','SMUKLA','2010-11-19','OBRZUCILA SZYSZKAMI');
INSERT INTO Wrogowie_Kocurow Values ('KURKA','BUREK','2010-12-14','POGONIL');
INSERT INTO Wrogowie_Kocurow Values ('MALY','CHYTRUSEK','2011-07-13','PODEBRAL PODEBRANE');
INSERT INTO Wrogowie_Kocurow Values ('UCHO','SWAWOLNY DYZIO','2011-07-14','OBRZUCIL KAMIENIAMI');


ALTER TABLE KOCURY DISABLE Constraint szef_fk;
INSERT INTO Kocury Values('JACEK','M','PLACEK','LOWCZY','LYSY','2008-12-01',67,NULL,2);
INSERT INTO Kocury Values('BARI','M','RURA','LAPACZ','LYSY','2009-09-01',56,NULL,2);
INSERT INTO Kocury Values('MICKA','D','LOLA','MILUSIA','TYGRYS','2009-10-14',25,47,1);
INSERT INTO Kocury Values('LUCEK','M','ZERO','KOT','KURKA','2010-03-01',43,NULL,3);
INSERT INTO Kocury Values('SONIA','D','PUSZYSTA','MILUSIA','ZOMBI','2010-11-18',20,35,3);
INSERT INTO Kocury Values('LATKA','D','UCHO','KOT','RAFA','2011-01-01',40,NULL,4);
INSERT INTO Kocury Values('DUDEK','M','MALY','KOT','RAFA','2011-05-15',40,NULL,4);
INSERT INTO Kocury Values('MRUCZEK','M','TYGRYS','SZEFUNIO',NULL,'2002-01-01',103,33,1);
INSERT INTO Kocury Values('CHYTRY','M','BOLEK','DZIELCZY','TYGRYS','2002-05-05',50,NULL,1);
INSERT INTO Kocury Values('KOREK','M','ZOMBI','BANDZIOR','TYGRYS','2004-03-16',75,13,3);
INSERT INTO Kocury Values('BOLEK','M','LYSY','BANDZIOR','TYGRYS','2006-08-15',72,21,2);
INSERT INTO Kocury Values('ZUZIA','D','SZYBKA','LOWCZY','LYSY','2006-07-21',65,NULL,2);
INSERT INTO Kocury Values('RUDA','D','MALA','MILUSIA','TYGRYS','2006-09-17',22,42,1);
INSERT INTO Kocury Values('PUCEK','M','RAFA','LOWCZY','TYGRYS','2006-10-15',65,NULL,4);
INSERT INTO Kocury Values('PUNIA','D','KURKA','LOWCZY','ZOMBI','2008-01-01',61,NULL,3);
INSERT INTO Kocury Values('BELA','D','LASKA','MILUSIA','LYSY','2008-02-01',24,28,2);
INSERT INTO Kocury Values('KSAWERY','M','MAN','LAPACZ','RAFA','2008-07-12',51,NULL,4);
INSERT INTO Kocury Values('MELA','D','DAMA','LAPACZ','RAFA','2008-11-01',51,NULL,4);
ALTER TABLE KOCURY ENABLE Constraint szef_fk;


--Zad1: Znajd� imiona wrog�w, kt�rzy dopu�cili si� incydent�w w 2009r.
SELECT imie_wroga as wrog ,OPIS_INCYDENTU as przewinienie
FROM wrogowie_kocurow
WHERE data_incydentu BETWEEN '2009-01-01' AND '2009-12-31';

--Zad2: Znajd� wszystkie kotki (p�e� �e�ska), kt�re przyst�pi�y do stada mi�dzy 
--1 wrze�nia 2005r. a 31 lipca 2007r.
SELECT imie, funkcja, w_stadku_od as "Z nami od"
FROM kocury
WHERE plec='D' AND w_stadku_od >= '2005-09-01' AND w_stadku_od <= '2007-07-31';

--Zad3: Wy�wietl imiona, gatunki i stopnie wrogo�ci nieprzekupnych wrog�w. 
--Wyniki maj� by� uporz�dkowane rosn�co wed�ug stopnia wrogo�ci
SELECT imie_wroga as WROG, gatunek, stopien_wrogosci as "STOPIEN WROGOSCI"
FROM wrogowie
WHERE Lapowka IS NULL
ORDER BY stopien_wrogosci;

--Zad4: Wy�wietli� dane o kotach p�ci m�skiej zebrane w jednej kolumnie postaci:
SELECT imie ||' zwany ' || pseudo || ' (fun.' || funkcja ||') lowi myszki w bandzie '
|| nr_bandy ||' od ' || w_stadku_od as "Wszystko o Kocurach"
FROM kocury
WHERE Plec='M'
ORDER BY w_stadku_od DESC, pseudo ASC;

--Zad5: . Znale�� pierwsze wyst�pienie litery A i pierwsze wyst�pienie litery L w 
--ka�dym pseudonimie a nast�pnie zamieni� znalezione litery na odpowiednio # i %. 
--Wykorzysta� funkcje dzia�aj�ce na �a�cuchach. Bra� pod uwag� tylko te imiona, 
--w kt�rych wyst�puj� obie litery.
SELECT pseudo,
(Case
  When pseudo LIKE '%A%L%' THEN     substr(  substr(pseudo,1,INSTR(pseudo,'A')-1) || '#' || substr(pseudo,INSTR(pseudo,'A')+1)  ,1,INSTR(pseudo,'L')-1 )|| '%' || substr(pseudo,INSTR(pseudo,'L')+1) 
  When pseudo LIKE '%L%A%' THEN     substr(  substr(pseudo,1,INSTR(pseudo,'L')-1) || '%' || substr(pseudo,INSTR(pseudo,'L')+1)  ,1,INSTR(pseudo,'A')-1 )|| '#' || substr(pseudo,INSTR(pseudo,'A')+1)
END) AS "Po zamianie"
FROM kocury
Where pseudo LIKE '%L%A%' OR  pseudo LIKE '%A%L%';

 -- substr(pseudo,1,INSTR(pseudo,'A')-1) || '#' ||  substr(pseudo,INSTR(pseudo,'#')+1, INSTR(pseudo,'L')-1) ||  '%' ||  substr(pseudo,INSTR(pseudo,'%')+1)      
 -- REGEXP_REPLACE(pseudo, INSTR(pseudo,'A'), '#')
--When pseudo LIKE '%L%A%' THEN  substr(pseudo,1,INSTR(pseudo,'L')-1) || '%' || substr(pseudo,INSTR(pseudo,'L')+1) 


--Zad6: Wy�wietli� imiona kot�w z co najmniej pi�cioletnim sta�em 
--(kt�re dodatkowo przyst�powa�y do stada od 1 marca do 30 wrze�nia), 
--daty ich przyst�pienia do stada, pocz�tkowy przydzia� myszy (obecny przydzia�
--ze wzgl�du na podwy�k� po p� roku cz�onkostwa,  jest o 10% wy�szy od pocz�tkowego) , 
--dat� wspomnianej podwy�ki o 10% oraz aktualnym przydzia� myszy. Wykorzysta� odpowiednie
--funkcje dzia�aj�ce na datach. W poni�szym rozwi�zaniu dat� bie��c� jest 06.02.2014
SELECT Imie,W_STADKU_OD as "W stadku",CEIL((PRZYDZIAL_MYSZY - 1/10 * PRZYDZIAL_MYSZY)) as "ZJADAL", ADD_MONTHS(W_STADKU_OD, 6) AS Podwyzka, PRZYDZIAL_MYSZY AS Zjada
FROM Kocury
WHERE EXTRACT(MONTH FROM W_STADKU_OD) BETWEEN 3 AND 9 AND 
MONTHS_BETWEEN('2014-02-06',W_STADKU_OD) >= 60  -- za date wstawic sysdate
ORDER BY Zjada DESC;


--Zad7: Wy�wietli� imiona, kwartalne przydzia�y myszy i kwartalne przydzia�y dodatkowe 
--dla wszystkich kot�w, u kt�rych przydzia� myszy jest wi�kszy od dwukrotnego przydzia�u 
--dodatkowego ale nie mniejszy od 55.
SELECT Imie,NVL(Przydzial_myszy*3,0) AS "Myszy Kwartalnie", NVL(Myszy_Ekstra*3,0) AS "Kwartalne Dodatki"
FROM Kocury
WHERE NVL(Przydzial_myszy,0) > NVL(2* Myszy_ekstra,0) AND NVL(Przydzial_myszy,0) >=55 
ORDER BY "Myszy Kwartalnie" DESC; 


--Zad8: Wy�wietli� dla ka�dego kota (imi�) nast�puj�ce informacje o ca�kowitym 
--rocznym spo�yciu myszy: warto�� ca�kowitego spo�ycia je�li przekracza 660,
--�Limit� je�li jest r�wne 660, �Ponizej 660� je�li jest mniejsze od 660. 
--Nie u�ywa� operator�w zbiorowych (UNION, INTERSECT, MINUS).

DEFINE a = '(NVL(Kocury.Przydzial_myszy,0) + NVL(Kocury.Myszy_ekstra,0))*12'
SELECT imie,
(CASE 
  WHEN  &a=660 THEN 'LIMIT'
  WHEN  &a < 660 THEN 'PONIZEJ 660'
  ELSE  TO_CHAR(&a)
END) AS "Zjada Rocznie"
FROM Kocury
ORDER by imie;

--Zad9:  Po kilkumiesi�cznym, spowodowanym kryzysem, zamro�eniu wydawania myszy
--Tygrys z dniem bie��cym wznowi� wyp�aty zgodnie z zasad�, �e koty, kt�re
--przyst�pi�y do stada w pierwszej po�owie miesi�ca (��cznie z 15-m) 
--otrzymuj� pierwszy po przerwie przydzia� myszy w ostatni� �rod� bie��cego
--miesi�ca, natomiast koty, kt�re przyst�pi�y do stada po 15-ym, pierwszy po 
--przerwie przydzia� myszy otrzymuj� w ostatni� �rod� nast�pnego miesi�ca.
--W kolejnych miesi�cach myszy wydawane s� wszystkim kotom w ostatni� �rod� 
--ka�dego miesi�ca. Wy�wietli� dla ka�dego kota jego pseudonim, dat� przyst�pienia 
--do stada oraz dat� pierwszego po przerwie przydzia�u myszy, przy za�o�eniu, �e  
--dat� bie��c� jest 24 i 27 marzec 2014.


Select PSEUDO, W_STADKU_OD as "W stadku", --po 27 nei ma srody a po 24 jest . 19,26,02 to sroda
(CASE 
 WHEN EXTRACT(DAY FROM W_STADKU_OD) <= 15 AND EXTRACT(MONTH FROM NEXT_DAY(to_date('2014-03-24'), 3)) = EXTRACT(MONTH FROM to_date('2014-03-24'))
 THEN NEXT_DAY(LAST_DAY('2014-03-24')-7, 3 )
 ELSE  NEXT_DAY(LAST_DAY(LAST_DAY( '2014-03-24')+1)-7,3)
END) AS WYPLATA
FROM Kocury
ORDER BY "W stadku";

Select PSEUDO, W_STADKU_OD as "W stadku",
(CASE 
 WHEN EXTRACT(DAY FROM W_STADKU_OD) <= 15 AND EXTRACT(DAY FROM NEXT_DAY(to_date('2014-03-27'), 3)) - EXTRACT(DAY FROM to_date('2014-03-27')) >= 0 
 THEN NEXT_DAY(LAST_DAY('2014-03-27')-7, 3 )
 ELSE  NEXT_DAY(LAST_DAY(LAST_DAY( '2014-03-27')+1)-7,3)
END) AS WYPLATA
FROM Kocury
ORDER BY "W stadku";

--Zad10: Atrybut pseudo w tabeli Kocury jest kluczem g��wnym tej tabeli. 
--Sprawdzi�, czy rzeczywi�cie wszystkie pseudonimy s� wzajemnie r�ne. 
--Zrobi� to samo dla atrybutu szef.

SELECT 
(CASE 
  WHEN COUNT(pseudo) = 1 THEN pseudo || ' - Unikalny' 
  ELSE pseudo || ' - nieunikalny' 
  END) AS "Unikalnosc atr. SZEF"
FROM kocury
GROUP BY pseudo;

SELECT 
(CASE 
  WHEN COUNT(szef) = 1 THEN szef || ' - Unikalny' 
  ELSE szef || ' - nieunikalny' 
  END) AS "Unikalnosc atr. SZEF"
FROM kocury
WHERE szef IS NOT NULL
GROUP BY szef;


--Zad11: Znale�� pseudonimy kot�w posiadaj�cych co najmniej dw�ch wrog�w.

SELECT pseudo , COUNT(pseudo) AS "Liczba wrogow"
FROM WROGOWIE_KOCUROW
GROUP BY pseudo
HAVING COUNT(pseudo) >=2
ORDER BY pseudo;

--Zad12: Znale�� maksymalny ca�kowity przydzia� myszy dla wszystkich grup 
--funkcyjnych (z pomini�ciem SZEFUNIA i kot�w p�ci m�skiej) o �rednim ca�kowitym 
--przydziale (z uwzgl�dnieniem dodatkowych przydzia��w � myszy_extra) wi�kszym od 50. 

SELECT 'Liczba kotow= ' || COUNT(funkcja) ||' lowi jako ' || funkcja || ' i zjada max. ' 
|| MAX(NVL(przydzial_myszy,0) + NVL(myszy_ekstra,0)) || ' myszy miesiecznie' as " "
FROM Kocury
Where funkcja <> 'SZEFUNCIO' AND Plec='D'
GROUP by funkcja
HAVING AVG(NVL(przydzial_myszy,0) + NVL(myszy_ekstra,0)) >50;

--Zad13: Wy�wietli� minimalny przydzia� myszy w ka�dej bandzie z podzia�em na p�cie.
SELECT NR_BANDY AS "Nr bandy", plec, MIN(NVL(przydzial_myszy,0)) AS "Minimlany przydzial"
FROM kocury
GROUP BY NR_BANDY, plec;


--Zad14: Wy�wietli� informacj� o kocurach (p�e� m�ska) posiadaj�cych w hierarchii 
--prze�o�onych szefa pe�ni�cego funkcj� BANDZIOR (wy�wietli� tak�e dane tego prze�o�onego). 
--Dane kot�w podleg�ych konkretnemu szefowi maj� by� wy�wietlone zgodnie z ich
--miejscem w hierarchii podleg�o�ci.
SELECT level AS "Poziom", pseudo, funkcja, Nr_bandy AS "Nr Bandy"
FROM kocury
WHERE plec = 'M'
CONNECT BY PRIOR pseudo = szef -- jak budowane jest drzewo   prior- wskazanie atrybutu nadrzednego do porownania
START WITH funkcja = 'BANDZIOR'; -- korzen drzewa

--Zad15: Przedstawi� informacj� o podleg�o�ci kot�w posiadaj�cych dodatkowy 
--przydzia� myszy tak aby imi� kota stoj�cego najwy�ej w hierarchii by�o 
--wy�wietlone z najmniejszym wci�ciem a pozosta�e imiona z wci�ciem odpowiednim 
--do miejsca w hierarchii.
SELECT LPAD(level-1,3*(level-1)+1, '==>') || ' '||imie as "Hierarchia",
NVL(szef,'Sam sobie panem') AS "Pseudo szefa", funkcja as "Funkcja"
FROM kocury
WHERE NVL(myszy_ekstra,0) > 0
CONNECT BY PRIOR pseudo=szef
START WITH szef IS NULL;


--Zad16: Wy�wietli� okre�lon� pseudonimami drog� s�u�bow� 
--(przez wszystkich kolejnych prze�o�onych do g��wnego szefa) kot�w p�ci m�skiej
--o sta�u d�u�szym ni� pi�� lat (w poni�szym rozwi�zaniu dat� bie��c� jest 06.02.2014) 
--nie posiadaj�cych dodatkowego przydzia�u myszy.
SELECT LPAD(' ', 2*level-1) || pseudo as "Droga sluzbowa"
FROM kocury
CONNECT BY PRIOR szef=pseudo
START WITH plec='M' and MONTHS_BETWEEN('2014-02-06', w_stadku_od) > 60 and NVL(myszy_ekstra,0)=0;


/* czy number moze byc null?
Create Table a
(
 ccc number(2)
);

INSERT INTO a VALUES(5);
INSERT INTO a VALUES(null);
INSERT INTO a VALUES(0);

SElect ccc
From a;

Drop table a;
*/