SET SERVEROUTPUT ON;

--Zad 34 
DECLARE 
  l_funkcja NUMBER;
  f Funkcje.funkcja%TYPE:='&funkcja';
  nie_znaleziono EXCEPTION;
BEGIN
  SELECT COUNT(*)
  INTO l_funkcja
  FROM Kocury K
  WHERE K.funkcja=f;
  IF l_funkcja>0
    THEN DBMS_OUTPUT.PUT_LINE('Znaleziono kota o funkcji ' || f);
  ELSE RAISE nie_znaleziono;
  END IF;
EXCEPTION 
  WHEN nie_znaleziono THEN DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota o funkcji ' || f);
  WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

--Zad 35 
DECLARE
  pm NUMBER; --calkowity przydzial myszy
  m VARCHAR2(20); --miesi�c st�pienia
  im Kocury.imie%TYPE;
  ps Kocury.pseudo%TYPE:='&pseudo';
BEGIN
  SELECT 12*(NVL(przydzial_myszy,0)+NVL(myszy_extra,0)), imie, TO_CHAR(EXTRACT(MONTH FROM w_stadku_od))
  INTO pm,im,m
  FROM Kocury
  WHERE pseudo=ps;
  IF pm>700 THEN DBMS_OUTPUT.PUT_LINE(im || ' - calkowity roczny przydzial myszy>700'); END IF;
  IF im LIKE '%A%' THEN DBMS_OUTPUT.PUT_LINE(im || ' - imie zawiera litere A'); END IF;
  IF m='1' THEN DBMS_OUTPUT.PUT_LINE(im || ' - stycze� jest miesi�cem przyst�pienia do stada');  END IF;
  IF pm<700 AND im NOT LIKE '%A' AND m<>'STYCZEN' THEN DBMS_OUTPUT.PUT_LINE(im || ' - nie odpowiada kryteriom'); END IF;
EXCEPTION
  WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

--Zad 36 
SELECT imie, przydzial_myszy "Myszki przed podwyzce" FROM Kocury ORDER BY przydzial_myszy DESC;

DECLARE
  ob_przy NUMBER:=0;
  licznik NUMBER:=0;
  fmax NUMBER:=0;
BEGIN
LOOP 
  EXIT WHEN ob_przy>1050;
  FOR k IN (SELECT imie, przydzial_myszy, funkcja
                FROM Kocury
                ORDER BY przydzial_myszy
                FOR UPDATE OF przydzial_myszy)
  LOOP 
    SELECT SUM(przydzial_myszy) INTO ob_przy FROM Kocury;
    EXIT WHEN ob_przy>1050;
    SELECT max_myszy INTO fmax FROM Funkcje WHERE funkcja = k.funkcja;
    IF k.przydzial_myszy<fmax THEN
      UPDATE Kocury SET przydzial_myszy = (CASE WHEN przydzial_myszy*1.1>fmax THEN fmax ELSE (przydzial_myszy*1.1) END)
      WHERE imie = k.imie;
      licznik:=licznik+1;
    END IF;
  END LOOP;
END LOOP;
	DBMS_OUTPUT.PUT_LINE('Calkowity przydzial w stadku ' || ob_przy|| ' Zmian - ' || licznik || '.');
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

SELECT imie, przydzial_myszy "Myszki po podwyzce" FROM Kocury ORDER BY przydzial_myszy DESC;

ROLLBACK;

--Zad 37
DECLARE
  TYPE rec_da IS RECORD (nr NUMBER, ps VARCHAR2(20), przydzial NUMBER);
  TYPE tab_da IS TABLE OF rec_da INDEX BY BINARY_INTEGER;
  tab_re tab_da;
  i BINARY_INTEGER;
BEGIN
  DBMS_OUTPUT.PUT_LINE(rpad('Nr',4)|| rpad('Pseudonim',12) || 'Zjada');
  DBMS_OUTPUT.PUT_LINE(LPAD('-',22,'-'));
  FOR i IN 1..5
  LOOP
    BEGIN
    SELECT * INTO tab_re(i) FROM 
      (SELECT ROW_NUMBER() OVER (ORDER BY NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0) DESC) nr, pseudo, NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0) 
      FROM Kocury)
      WHERE nr=i;
      DBMS_OUTPUT.PUT_LINE(RPAD(tab_re(i).nr,4) || RPAD(tab_re(i).ps,12) || LPAD(tab_re(i).przydzial,5));
    END;
  END LOOP;
  EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

--Zad 38
DECLARE
  CURSOR k IS SELECT imie,pseudo, szef FROM Kocury WHERE funkcja='KOT' OR funkcja='MILUSIA';
  k_rec k%ROWTYPE; 
  ps Kocury.pseudo%TYPE;
  ks Kocury.pseudo%TYPE;
  sz Kocury.pseudo%TYPE;
  im Kocury.imie%TYPE;
  n NUMBER:='&Przelozeni';
  i BINARY_INTEGER;
BEGIN 
  OPEN k;
  DBMS_OUTPUT.PUT(RPAD('Imie',13));
  FOR i IN 1..n
    LOOP
      DBMS_OUTPUT.PUT(RPAD(' | ' || ' Szef ' || i,17));
    END LOOP;
      DBMS_OUTPUT.PUT_LINE('');
      DBMS_OUTPUT.PUT(LPAD('-',12,'-'));
  FOR i IN 1..n
    LOOP
      DBMS_OUTPUT.PUT(' --- ');
      DBMS_OUTPUT.PUT(LPAD('-',12,'-'));
    END LOOP;
      DBMS_OUTPUT.PUT_LINE('');
    LOOP
      FETCH k INTO k_rec;
      EXIT WHEN k%NOTFOUND;
          DBMS_OUTPUT.PUT(RPAD(k_rec.imie,13));
          ks:=k_rec.szef;
          FOR i IN 1..n
          LOOP
            IF ks IS NULL THEN
                   DBMS_OUTPUT.PUT(' |  ' || RPAD(' ',13));
            ELSE SELECT pseudo,imie,szef INTO ps,im,sz FROM Kocury WHERE pseudo=ks;
                  ks:=sz;
                  DBMS_OUTPUT.PUT(' |  ' || RPAD(im,13));
            END IF;
            END LOOP;
            DBMS_OUTPUT.PUT_LINE('');
  END LOOP;
  CLOSE k;
  EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

--Zad 39
DECLARE 
  new_nr VARCHAR2(20):='&nr_bandy';
  new_nazwa VARCHAR2(20):='&nazwa';
  new_teren VARCHAR2(20):='&teren';
  e_nr BOOLEAN:=FALSE;
  e_n BOOLEAN:=FALSE;
  e_t BOOLEAN:=FALSE;
  exists_val EXCEPTION;
  incorrect_nr EXCEPTION;
BEGIN
  FOR k IN (SELECT nr_bandy, nazwa, teren FROM Bandy)
  LOOP
    IF k.nr_bandy=new_nr THEN e_nr:=TRUE; END IF;
    IF new_nr<=0 THEN RAISE incorrect_nr; END IF;
    IF k.nazwa=new_nazwa THEN e_n:=TRUE; END IF;
    IF k.teren=new_teren THEN e_t:=TRUE;  END IF;
  END LOOP;
    IF e_nr=TRUE THEN DBMS_OUTPUT.PUT(new_nr); END IF;
    IF e_n=TRUE THEN DBMS_OUTPUT.PUT(CASE WHEN e_nr=TRUE THEN ', ' END); DBMS_OUTPUT.PUT(new_nazwa); END IF;
    IF e_t=TRUE THEN DBMS_OUTPUT.PUT(CASE WHEN e_n=TRUE OR e_nr=TRUE THEN ', ' END); DBMS_OUTPUT.PUT(new_teren); END IF;
    IF e_nr=TRUE OR e_n=TRUE OR e_t=TRUE THEN RAISE exists_val; 
    ELSE INSERT INTO Bandy(nr_bandy, nazwa, teren) VALUES(new_nr, new_nazwa, new_teren); DBMS_OUTPUT.PUT_LINE('Dodano bande poprawnie.');
    END IF;
EXCEPTION
  WHEN exists_val THEN DBMS_OUTPUT.PUT_LINE(': juz istnieje');
  WHEN incorrect_nr THEN DBMS_OUTPUT.PUT_LINE('Nr bandy musi by� wi�kszy od 0!');
  WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

SELECT * FROM Bandy;

ROLLBACK;

--Zad 40
CREATE OR REPLACE PROCEDURE p40 (new_nr IN NUMBER, new_nazwa IN VARCHAR2, new_teren IN VARCHAR2) 
IS
  e_nr BOOLEAN:=FALSE;
  e_n BOOLEAN:=FALSE;
  e_t BOOLEAN:=FALSE;
  exists_val EXCEPTION;
  incorrect_nr EXCEPTION;
BEGIN
  FOR k IN (SELECT nr_bandy, nazwa, teren FROM Bandy)
  LOOP
    IF k.nr_bandy=new_nr THEN e_nr:=TRUE; END IF;
    IF new_nr<=0 THEN RAISE incorrect_nr; END IF;
    IF k.nazwa=new_nazwa THEN e_n:=TRUE; END IF;
    IF k.teren=new_teren THEN e_t:=TRUE;  END IF;
  END LOOP;
    IF e_nr=TRUE THEN DBMS_OUTPUT.PUT(new_nr); END IF;
    IF e_n=TRUE THEN DBMS_OUTPUT.PUT(CASE WHEN e_nr=TRUE THEN ', ' END); DBMS_OUTPUT.PUT(new_nazwa); END IF;
    IF e_t=TRUE THEN DBMS_OUTPUT.PUT(CASE WHEN e_n=TRUE OR e_nr=TRUE THEN ', ' END); DBMS_OUTPUT.PUT(new_teren); END IF;
    IF e_nr=TRUE OR e_n=TRUE OR e_t=TRUE THEN RAISE exists_val; 
    ELSE INSERT INTO Bandy(nr_bandy, nazwa, teren) VALUES(new_nr, new_nazwa, new_teren); DBMS_OUTPUT.PUT_LINE('Dodano bande poprawnie.');
    END IF;
EXCEPTION
  WHEN exists_val THEN DBMS_OUTPUT.PUT_LINE(': juz istnieje');
  WHEN incorrect_nr THEN DBMS_OUTPUT.PUT_LINE('Nr bandy musi by� wi�kszy od 0!');
  WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

EXEC p40(9,'TEST','TEST');
SELECT * FROM Bandy;
ROLLBACK;

--Zad 41

SET SERVEROUTPUT ON;

CREATE OR REPLACE TRIGGER z41
BEFORE INSERT ON Bandy
DECLARE
nr NUMBER;
BEGIN
SELECT MAX(nr_bandy) INTO nr FROM Bandy;
IF :NEW.nr_bandy - nr > 1
THEN :NEW.nr_bandy := nr + 1;
END IF;
END z41;

/
EXEC z40 (99, 'SLEPI', 'PIWNICA');

SELECT *
FROM Bandy;

ROLLBACK;
DROP TRIGGER z41;

EXEC p40(99,'TEST','TEST');
EXEC p40(99,'TEST2','TEST2');
SELECT * FROM Bandy;
ROLLBACK;

--Zad 42
CREATE OR REPLACE TRIGGER t42
FOR UPDATE ON Kocury
COMPOUND TRIGGER
   pm_tygrysa NUMBER;
   zly_szef BOOLEAN:=FALSE;
   dobry_szef BOOLEAN:=FALSE;   
   BEFORE STATEMENT IS
   BEGIN 
      SELECT przydzial_myszy INTO pm_tygrysa FROM Kocury WHERE pseudo='TYGRYS';
   END BEFORE STATEMENT;
   BEFORE EACH ROW IS
   BEGIN
    IF :old.funkcja='MILUSIA' THEN
      IF :new.przydzial_myszy-:old.przydzial_myszy<0 THEN
         :new.przydzial_myszy:=:old.przydzial_myszy;
         :new.myszy_ekstra:=:old.myszy_ekstra+5;
         zly_szef:=TRUE;
      ELSIF :new.przydzial_myszy-:old.przydzial_myszy<pm_tygrysa*0.1 THEN
         :new.przydzial_myszy:=:new.przydzial_myszy;
         :new.myszy_ekstra:=:old.myszy_ekstra+5;
         zly_szef:=TRUE;
      ELSE
         dobry_szef:=TRUE;
      END IF;
    END IF;
    END BEFORE EACH ROW;
    AFTER STATEMENT IS
    BEGIN
      IF zly_szef THEN
        UPDATE Kocury SET przydzial_myszy=przydzial_myszy-przydzial_myszy*0.1 WHERE pseudo='TYGRYS';
        zly_szef:=FALSE;
      ELSIF dobry_szef THEN
        UPDATE Kocury SET myszy_ekstra=myszy_ekstra+5 WHERE pseudo='TYGRYS';
        dobry_szef:=FALSE;
      END IF;
    END AFTER STATEMENT;
END t42;

SELECT * FROM Kocury;
UPDATE Kocury
SET przydzial_myszy=przydzial_myszy+1;
SELECT * FROM Kocury;
ROLLBACK;

--Zad 43 
DECLARE
  znaleziono BOOLEAN:=FALSE;
  suma NUMBER:=0;
  totalSuma NUMBER:=0;
  sumaF NUMBER:=0;
  ile NUMBER:=0;
  CURSOR  cf IS SELECT funkcja FROM Funkcje;
BEGIN
  DBMS_OUTPUT.PUT(RPAD('NAZWA BANDY',17) || RPAD(' PLEC',7));
  FOR k IN cf
  LOOP
      DBMS_OUTPUT.PUT(LPAD(k.funkcja,12));
  END LOOP;
      DBMS_OUTPUT.PUT_LINE(LPAD('ILE ',7) || LPAD('SUMA',7) || ' ');
      DBMS_OUTPUT.PUT(RPAD('-',17,'-') || ' ' || LPAD('-',7,'-')  || ' ');
  FOR k IN cf
    LOOP
      DBMS_OUTPUT.PUT(RPAD('-',11,'-') || ' ');
    END LOOP;
      DBMS_OUTPUT.PUT(LPAD('-',4,'-') || ' ' || LPAD('-',7,'-'));
      DBMS_OUTPUT.PUT_LINE('');
    FOR k IN (SELECT B.nazwa nb, K1.plec pl FROM Kocury K1 JOIN Bandy B ON B.nr_bandy=K1.nr_bandy GROUP BY B.nazwa, K1.plec ORDER BY B.nazwa, K1.plec)
      LOOP
        IF k.pl='D' THEN  
          DBMS_OUTPUT.PUT(RPAD(k.nb || ' ',15));
          DBMS_OUTPUT.PUT(LPAD('Kotka ',9)); 
        ELSE
          DBMS_OUTPUT.PUT(RPAD(' ',15));
          DBMS_OUTPUT.PUT(LPAD('Kocur ',9)); END IF;
       FOR f1 IN cf
        LOOP
          znaleziono:=FALSE;
        FOR f2 IN (SELECT funkcja FROM Funkcje)
        LOOP
          IF f2.funkcja=f1.funkcja THEN
          FOR s IN (SELECT SUM(NVL(K1.przydzial_myszy,0)+NVL(K1.myszy_extra,0)) pm, funkcja FROM Kocury K1 
          JOIN Bandy B ON B.nr_bandy=K1.nr_bandy WHERE B.nazwa=k.nb AND K1.plec=k.pl AND K1.funkcja=f2.funkcja GROUP BY K1.funkcja)
          LOOP
              DBMS_OUTPUT.PUT(LPAD(s.pm || ' ',12));
              znaleziono:=TRUE;
              suma:=suma+s.pm;
              ile:=ile+1;
          END LOOP; END IF;
          END LOOP;
          IF znaleziono=FALSE THEN  DBMS_OUTPUT.PUT(LPAD('0 ',12)); END IF;
        END LOOP;
           DBMS_OUTPUT.PUT(LPAD(ile || ' ',7));
           DBMS_OUTPUT.PUT_LINE(LPAD(suma || ' ',7));
           totalSuma:=totalSuma+suma;
           ile:=0;
           suma:=0;
      END LOOP;
      DBMS_OUTPUT.PUT(RPAD('-',17,'-') || ' ' || LPAD('-',7,'-')  || ' ');
      FOR k IN cf
          LOOP
            DBMS_OUTPUT.PUT(RPAD('-',11,'-') || ' ');
          END LOOP;
            DBMS_OUTPUT.PUT(LPAD('-',4,'-') || ' ' || LPAD('-',7,'-'));
            DBMS_OUTPUT.PUT_LINE('');
    DBMS_OUTPUT.PUT(RPAD('Suma',24));
    FOR cur in cf
    LOOP
      SELECT SUM(NVL(K.przydzial_myszy,0)+NVL(K.myszy_extra,0)) INTO sumaF FROM Kocury K 
      RIGHT JOIN Funkcje F ON K.funkcja=F.funkcja WHERE cur.funkcja=F.funkcja GROUP BY F.funkcja;
      DBMS_OUTPUT.PUT(LPAD(sumaF || ' ',12));
    END LOOP;
    DBMS_OUTPUT.PUT_LINE(LPAD(totalSuma,13));
END;

--Zad 44
CREATE OR REPLACE FUNCTION f44 (m Kocury.pseudo%TYPE, dodatek NUMBER:=0) 
RETURN NUMBER AS podatek NUMBER;
BEGIN
  DECLARE
    ps Kocury.pseudo%TYPE;
    pm NUMBER; -- przydzial myszy
    w NUMBER; --liczba wrog�w
    pw NUMBER; --liczba podwadnych
  BEGIN
    SELECT K.pseudo, MIN(K.przydzial_myszy+NVL(K.myszy_extra,0)), COUNT(DISTINCT WK.imie_wroga) 
    INTO ps,pm, w
    FROM Kocury K LEFT JOIN Wrogowie_Kocurow WK ON K.pseudo=WK.pseudo
    WHERE K.pseudo=m
    GROUP BY K.pseudo;
    SELECT COUNT(DISTINCT pseudo) INTO pw FROM Kocury WHERE szef=m; 
    podatek:=CEIL(pm*0.05);
    IF pw<1 THEN podatek:=podatek+2; END IF;
    IF w<1 THEN podatek:=podatek+1; END IF;
    podatek:=podatek+dodatek;
  END;
  RETURN podatek;
END;

select f44('LOLA') from dual;
select f44('LOLA',2) from dual;

CREATE OR REPLACE PACKAGE pakiet44 AS
FUNCTION f44(m Kocury.pseudo%TYPE, dodatek NUMBER:=0) RETURN NUMBER;
PROCEDURE p40(new_nr IN NUMBER, new_nazwa IN VARCHAR2, new_teren IN VARCHAR2);
END pakiet44;

CREATE OR REPLACE PACKAGE BODY pakiet44 AS
FUNCTION f44(m Kocury.pseudo%TYPE, dodatek NUMBER:=0) RETURN NUMBER IS
  podatek NUMBER;
BEGIN
  DECLARE
    ps Kocury.pseudo%TYPE;
    pm NUMBER; -- przydzial myszy
    w NUMBER; --liczba wrog�w
    pw NUMBER; --liczba podwadnych
  BEGIN
    SELECT K.pseudo, MIN(K.przydzial_myszy+NVL(K.myszy_extra,0)), COUNT(DISTINCT WK.imie_wroga) 
    INTO ps,pm, w
    FROM Kocury K LEFT JOIN Wrogowie_Kocurow WK ON K.pseudo=WK.pseudo
    WHERE K.pseudo=m
    GROUP BY K.pseudo;
    SELECT COUNT(DISTINCT pseudo) INTO pw FROM Kocury WHERE szef=m; 
    podatek:=CEIL(pm*0.05);
    IF pw<1 THEN podatek:=podatek+2; END IF;
    IF w<1 THEN podatek:=podatek+1; END IF;
    podatek:=podatek+dodatek;
  END;
  RETURN podatek;
END f44;
PROCEDURE p40(new_nr IN NUMBER, new_nazwa IN VARCHAR2, new_teren IN VARCHAR2) IS
    e_nr BOOLEAN:=FALSE;
    e_n BOOLEAN:=FALSE;
    e_t BOOLEAN:=FALSE;
    exists_val EXCEPTION;
    incorrect_nr EXCEPTION;
  BEGIN
    FOR k IN (SELECT nr_bandy, nazwa, teren FROM Bandy)
    LOOP
      IF k.nr_bandy=new_nr THEN e_nr:=TRUE; END IF;
      IF new_nr<=0 THEN RAISE incorrect_nr; END IF;
      IF k.nazwa=new_nazwa THEN e_n:=TRUE; END IF;
      IF k.teren=new_teren THEN e_t:=TRUE;  END IF;
    END LOOP;
      IF e_nr=TRUE THEN DBMS_OUTPUT.PUT(new_nr); END IF;
      IF e_n=TRUE THEN DBMS_OUTPUT.PUT(CASE WHEN e_nr=TRUE THEN ', ' END); DBMS_OUTPUT.PUT(new_nazwa); END IF;
      IF e_t=TRUE THEN DBMS_OUTPUT.PUT(CASE WHEN e_n=TRUE OR e_nr=TRUE THEN ', ' END); DBMS_OUTPUT.PUT(new_teren); END IF;
      IF e_nr=TRUE OR e_n=TRUE OR e_t=TRUE THEN RAISE exists_val; 
      ELSE INSERT INTO Bandy(nr_bandy, nazwa, teren) VALUES(new_nr, new_nazwa, new_teren); DBMS_OUTPUT.PUT_LINE('Dodano bande poprawnie.');
      END IF;
  EXCEPTION
    WHEN exists_val THEN DBMS_OUTPUT.PUT_LINE(': juz istnieje');
    WHEN incorrect_nr THEN DBMS_OUTPUT.PUT_LINE('Nr bandy musi by� wi�kszy od 0!');
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
  END p40;
END pakiet44;

SELECT pseudo, pakiet44.f44(pseudo) FROM Kocury;
SELECT pseudo, pakiet44.f44(pseudo,2) FROM Kocury;





/*
--Zad 45 asi
CREATE TABLE Dodatki_extra
  (pseudo VARCHAR2(15) CONSTRAINTS dodatki_fk_kocury REFERENCES Kocury(pseudo),
  dodatek_extra NUMBER(3)
);

DECLARE
  CURSOR milusie IS SELECT pseudo, myszy_extra FROM Kocury WHERE funkcja='MILUSIA';
BEGIN
  FOR m IN milusie
  LOOP
    INSERT INTO Dodatki_extra VALUES(m.pseudo, m.myszy_extra);
  END LOOP;
    INSERT INTO Dodatki_extra VALUES('TYGRYS', 33);
END;

SELECT * FROM Dodatki_extra;
 
CREATE OR REPLACE TRIGGER t45
AFTER UPDATE OF przydzial_myszy ON Kocury
FOR EACH ROW
DECLARE
  uzytkownik VARCHAR2(20):= LOGIN_USER;
  dyn VARCHAR2(200);
BEGIN
  IF uzytkownik!='TYGRYS' THEN
    dyn:='UPDATE Dodatki_extra SET dodatek_extra=dodatek_extra-10 WHERE pseudo=:pseudo';
    IF ((:new.przydzial_myszy-:old.przydzial_myszy>0) AND :old.funkcja='MILUSIA') THEN
      EXECUTE IMMEDIATE dyn USING :old.pseudo;
    END IF;
  END IF;
  DBMS_OUTPUT.PUT_LINE(uzytkownik);
END;
 
UPDATE Kocury SET przydzial_myszy=przydzial_myszy+1;

SELECT * FROM Dodatki_extra;
ROLLBACK;
DROP TRIGGER t45;
DROP TABLE Dodatki_extra;*/



















/*
na pewno ostatnie bo zrobi�am tak �e najpierw doda�am do dodatk�w_extra 
wszystkie milusie i p�niej robi�am update a mia�o by� tak �e tylko kolejne 
wspisy si� wstawia�o. w sensie kolejne krotki milusia i -10 czy jako� tak


najpierw napisz� ci kt�re zadania mnie pyta� i na kt�re musisz uwa�a�, 
o pozosta�e mnie nie pyta� wi�c mo�esz je wzi��

zad 41 Pytanie dlaczego FOR EACH ROW

zad 42 Pytanie odno�nie COMPOUND (s� dwa sposoby rozwi�zania). 
Compound jest od kt�rej� wersji 11, poczytaj o tym, w ten spos�b jest o 
wiele pro�ciej, mo�na to te� zrobi� z u�yciem chyba dw�ch trigger�w.

zad 43 Pytanie co i jak + Testy je�li dodamy now� funkcj�. chyba
w tym zadaniu musia�am przy nim testowa� i doda� jak�� funkcj� 
i przetestowa�

zad 45 Pytanie (troche inaczej zrobione mia� by� INSERT) 
+ TESTY czy dwa triggery dzia�aj� przy zmianie przydzia�u



te 4 chyba lepiej jak we�miesz od kogo� innego bo od kogo� 
z innej grupy to troch� inaczej zawsze


Wywal ostatnie!!!!!

chyba mnia�am co� jeszcze inaczej z jakim� trigerem gdzie trzeba by�o
uwzgl�dni� ten dodatkowy podatek i w jednym punkcie by�o �e osoba go
wywo�uj�ca ustala jeszcze jaki� dodatkowy podatek

do 43:
w jednym zadaniu jak mnie pyta� to zauwa�y� �e zrobi�am kursor po 
funkcjach troch� niepotrzebnie bo wtedy wyswietla mi funkcje w kt�rych 
nie ma �adnym kot�w i tabelka jest z samymi 0
mo�na by�o zrobi� bo kocurach kursor i wzi�� te wszystkie funkcje

*/