--(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=ORACLE.win.ii.pwr.wroc.pl)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)SERVICE_NAME=orcl.win.ii.pwr.wroc.pl)))
ALTER SESSION SET NLS_Date_Format = 'YYYY-MM-DD';
--SET SERVEROUTPUT ON;

/*
Zad. 34. Napisa� blok PL/SQL, kt�ry wybiera z relacji Kocury koty o
funkcji podanej z klawiatury. Jedynym efektem dzia�ania bloku ma by�
komunikat informuj�cy czy znaleziono, czy te� nie, kota pe�ni�cego podan�
funkcj� (w przypadku znalezienia kota wy�wietli� nazw� odpowiedniej funkcji). 
*/

DECLARE
  func Kocury.FUNKCJA%TYPE:='&funkcja';
  counted NUMBER;
  not_found EXCEPTION;
BEGIN
  SELECT COUNT(*) INTO counted FROM Kocury WHERE funkcja=func;
  IF counted=0 THEN
    RAISE not_found;
  ELSE
      DBMS_OUTPUT.PUT_LINE('Znaleziono kota o funkcji: ' || func);
  END IF;
EXCEPTION
  WHEN not_found THEN
    DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota pelni�cego dan� funkcj�');
END;

/*
Zad. 35. Napisa� blok PL/SQL, kt�ry wyprowadza na ekran nast�puj�ce 
informacje o kocie o pseudonimie wprowadzonym z klawiatury (w zale�no�ci 
od rzeczywistych danych):
-	'calkowity roczny przydzial myszy >700'
-	'imi� zawiera litere A'
-	'stycze� jest miesiacem przystapienia do stada'
-	'nie odpowiada kryteriom'.
Powy�sze informacje wymienione s� zgodnie z hierarchi� wa�no�ci.
Ka�d� wprowadzan� informacj� poprzedzi� imieniem kota.
*/


DECLARE
  pseudonim Kocury.PSEUDO%TYPE:='&pseudo';
  przydzial NUMBER;
  imieKota Kocury.IMIE%TYPE;
  miesiac NUMBER;
  b BOOLEAN:=FALSE;
BEGIN
  SELECT (przydzial_myszy+NVL(myszy_ekstra,0))*12, imie, EXTRACT(MONTH FROM w_stadku_od) INTO przydzial, imieKota, miesiac
  FROM Kocury WHERE pseudo=pseudonim;
  -- przydzial > 700
  IF przydzial > 700 THEN
    DBMS_OUTPUT.PUT_LINE(pseudonim||' calkowity roczny przydzial myszy >700');
    b:=TRUE;
  END IF;
  -- imie z A
  IF imieKota LIKE '%A%' THEN
    DBMS_OUTPUT.PUT_LINE(pseudonim||' imie zawiera litere A');
    b:=TRUE;
  END IF;
  -- styczen
  IF miesiac = 1 THEN
    DBMS_OUTPUT.PUT_LINE(pseudonim||' styczen jest miesiacem przystapienia do stada');
    b:=TRUE;
  END IF;
  -- nie odpowiada
  IF b = FALSE THEN
    DBMS_OUTPUT.PUT_LINE(pseudonim||' nie odpowiada kryteriom');
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE(pseudonim||' nie jest czlonkiem stada');
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

/*
Zad. 36. W zwi�zku z du�� wydajno�ci� w �owieniu myszy SZEFUNIO postanowi�
wynagrodzi� swoich podw�adnych. Og�osi� wi�c, �e podwy�sza indywidualny
przydzia� myszy ka�dego kota o 10% poczynaj�c od kot�w o najni�szym przydziale.
Je�li w kt�rym� momencie suma wszystkich przydzia��w przekroczy 1050, 
�aden inny kot nie dostanie podwy�ki. 
Je�li przydzia� myszy po podwy�ce przekroczy maksymaln� warto�� nale�n�
dla pe�nionej funkcji (relacja Funkcje), przydzia� myszy po podwy�ce 
ma by� r�wny tej warto�ci. 
Napisa� blok PL/SQL z kursorem, kt�ry wyznacza sum� przydzia��w przed podwy�k�
a realizuje to zadanie. Blok ma dzia�a� tak d�ugo,
a� suma wszystkich przydzia��w rzeczywi�cie przekroczy 1050 
(liczba �obieg�w podwy�kowych� mo�e by� wi�ksza od 1 a wi�c i podwy�ka mo�e by�
wi�ksza ni� 10%). Wy�wietli� na ekranie sum� przydzia��w myszy po wykonaniu
zadania wraz z liczb� podwy�ek (liczb� zmian w relacji Kocury). 
Na ko�cu wycofa� wszystkie zmiany.
*/
SELECT imie, przydzial_myszy "Myszki przed podwyzka" FROM Kocury ORDER BY przydzial_myszy DESC;

DECLARE
  ob_przy NUMBER:=0;
  licznik NUMBER:=0;
  fmax NUMBER:=0;
BEGIN
LOOP 
  EXIT WHEN ob_przy>1050;
  FOR k IN (SELECT imie, przydzial_myszy, funkcja
                FROM Kocury
                ORDER BY przydzial_myszy
                FOR UPDATE OF przydzial_myszy)
  LOOP 
    SELECT SUM(przydzial_myszy) INTO ob_przy FROM Kocury;
    EXIT WHEN ob_przy>1050;
    SELECT max_myszy INTO fmax FROM Funkcje WHERE funkcja = k.funkcja;
    IF k.przydzial_myszy<fmax THEN
      UPDATE Kocury 
      SET przydzial_myszy = (CASE WHEN (przydzial_myszy*1.1)>fmax THEN fmax ELSE (przydzial_myszy*1.1) END)
      WHERE imie = k.imie;
      licznik:=licznik+1;
    END IF;
  END LOOP;
END LOOP;
	DBMS_OUTPUT.PUT_LINE('Calkowity przydzial w stadku ' || ob_przy|| ' Zmian - ' || licznik || '.');
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

SELECT imie, przydzial_myszy "Myszki po podwyzce" FROM Kocury ORDER BY przydzial_myszy DESC;

ROLLBACK;

/*
Zad. 37. Napisa� blok, kt�ry powoduje wybranie w p�tli kursorowej FOR pi�ciu 
kot�w o najwy�szym ca�kowitym przydziale myszy. Wynik wy�wietli� na ekranie.
*/
DECLARE
counter number :=1;  
BEGIN
  DBMS_OUTPUT.PUT_LINE(rpad('Nr',4)|| rpad('Pseudonim',12) || rpad('Zjada',5));
  DBMS_OUTPUT.PUT_LINE(RPAD('-',22,'-'));
   FOR top IN (SELECT pseudo ps,(NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0)) Zjada
                FROM KOCURY             
                ORDER BY Zjada DESC)
  LOOP
      EXIT WHEN counter >5;
      DBMS_OUTPUT.PUT_LINE(RPAD(counter,4) || RPAD(top.ps,12) || RPAD(top.Zjada,5));
      counter:=counter+1;
  END LOOP;
END;

/*
Zad. 38. Napisa� blok, kt�ry zrealizuje zad. 19 w spos�b uniwersalny 
(bez konieczno�ci uwzgl�dniania wiedzy o liczbie prze�o�onych kota 
usytuowanego najni�ej w hierarchii). Dan� wej�ciow� ma by� maksymalna liczba 
wy�wietlanych prze�o�onych.
*/
DECLARE
  CURSOR k IS SELECT imie,pseudo, szef FROM Kocury WHERE funkcja='KOT' OR funkcja='MILUSIA';
  k_rec k%ROWTYPE; 
  ps Kocury.pseudo%TYPE;
  ks Kocury.pseudo%TYPE;
  sz Kocury.pseudo%TYPE;
  im Kocury.imie%TYPE;
  n NUMBER:='&Przelozeni';
  i BINARY_INTEGER;
BEGIN 
  OPEN k;
 DBMS_OUTPUT.PUT(RPAD('Imie',13));
  FOR i IN 1..n
  LOOP
      DBMS_OUTPUT.PUT(RPAD(' |' || ' Szef ' || i,17));
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('');
  FOR i IN 1..n
  LOOP
      DBMS_OUTPUT.PUT(LPAD('-',18,'-'));
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('');
  LOOP
    FETCH k INTO k_rec;
    EXIT WHEN k%NOTFOUND;
    DBMS_OUTPUT.PUT(RPAD(k_rec.imie,13));
    ks:=k_rec.szef;
    FOR i IN 1..n
    LOOP
    IF ks IS NULL THEN 
        DBMS_OUTPUT.PUT(' |  ' || RPAD(' ',13));
    ELSE 
        SELECT pseudo,imie,szef INTO ps,im,sz FROM Kocury WHERE pseudo=ks;
        ks:=sz;
        DBMS_OUTPUT.PUT(' |  ' || RPAD(im,13));
    END IF;
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('');
  END LOOP;
  CLOSE k;
  EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

/*
Zad. 39. Napisa� blok PL/SQL wczytuj�cy trzy parametry reprezentuj�ce nr bandy, 
nazw� bandy oraz teren polowa�. Skrypt ma uniemo�liwia� wprowadzenie 
istniej�cych ju� warto�ci parametr�w poprzez obs�ug� odpowiednich wyj�tk�w. 
Sytuacj� wyj�tkow� jest tak�e wprowadzenie numeru bandy <=0.
W przypadku zaistnienia sytuacji wyj�tkowej nale�y wyprowadzi� na ekran 
odpowiedni komunikat. W przypadku prawid�owych parametr�w nale�y stworzy� 
now� band� w relacji Bandy. Zmian� nale�y na ko�cu wycofa�.
*/
DECLARE 
  new_nr VARCHAR2(20):='&nr_bandy';
  new_nazwa VARCHAR2(20):='&nazwa';
  new_teren VARCHAR2(20):='&teren';
  exists_val EXCEPTION;
  incorrect EXCEPTION;
BEGIN
  FOR k IN (SELECT nr_bandy, nazwa, teren FROM Bandy)
  LOOP
    IF new_nr<=0 THEN RAISE incorrect; 
    ELSIF  k.nr_bandy=new_nr OR  k.nazwa=new_nazwa OR k.teren=new_teren THEN  RAISE exists_val;
    END IF;
  END LOOP;
  INSERT INTO Bandy(nr_bandy, nazwa, teren) VALUES(new_nr, new_nazwa, new_teren); DBMS_OUTPUT.PUT_LINE('Dodano bande poprawnie.');
EXCEPTION
  WHEN exists_val THEN DBMS_OUTPUT.PUT_LINE('Juz istnieje cz�� wprowadzonych danych. Wprowad� inne dane.');
  WHEN incorrect THEN DBMS_OUTPUT.PUT_LINE('Nr bandy musi by� wi�kszy od 0!');
  WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

SELECT * FROM Bandy;
ROLLBACK;

/* Zad. 40. Przerobi� blok z zadania 39 na procedur� umieszczon� 
w bazie danych. */
CREATE OR REPLACE PROCEDURE p40 (new_nr IN NUMBER, new_nazwa IN VARCHAR2, new_teren IN VARCHAR2) 
IS
  exists_val EXCEPTION;
  incorrect EXCEPTION;
BEGIN
  FOR k IN (SELECT nr_bandy, nazwa, teren FROM Bandy)
  LOOP
    IF new_nr<=0 THEN RAISE incorrect; 
    ELSIF  k.nr_bandy=new_nr OR  k.nazwa=new_nazwa OR k.teren=new_teren THEN  RAISE exists_val;
    END IF;
  END LOOP;
  INSERT INTO Bandy(nr_bandy, nazwa, teren) VALUES(new_nr, new_nazwa, new_teren); DBMS_OUTPUT.PUT_LINE('Dodano bande poprawnie.');
EXCEPTION
  WHEN exists_val THEN DBMS_OUTPUT.PUT_LINE('Juz istnieje cz�� wprowadzonych danych. Wprowad� inne dane.');
  WHEN incorrect THEN DBMS_OUTPUT.PUT_LINE('Nr bandy musi by� wi�kszy od 0!');
  WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;

EXEC p40(6,'AA','BB');
SELECT * FROM Bandy;
ROLLBACK;


/*Zad. 41. Zdefiniowa� wyzwalacz, kt�ry zapewni, �e numer nowej bandy b�dzie 
zawsze wi�kszy o 1 od najwy�szego numeru istniej�cej ju� bandy. 
Sprawdzi� dzia�anie wyzwalacza wykorzystuj�c procedur� z zadania 40.
*/

SET SERVEROUTPUT ON;

CREATE OR REPLACE TRIGGER p41
BEFORE INSERT ON Bandy
FOR EACH ROW
DECLARE
nr NUMBER;
BEGIN
SELECT MAX(nr_bandy)+1 INTO :NEW.nr_bandy FROM Bandy;
END p41;
/
EXEC p40 (80, 'AA', 'BB');

SELECT * FROM Bandy;
ROLLBACK;
DROP TRIGGER p41;

/*Zad. 42. Milusie postanowi�y zadba� o swoje interesy. 
Wynaj�y wi�c informatyka, aby zapu�ci� wirusa w system Tygrysa.
Teraz przy ka�dej pr�bie zmiany przydzia�u myszy na plus 
(o minusie w og�le nie mo�e by� mowy) o warto�� mniejsz� ni� 10% przydzia�u 
myszy Tygrysa �al Milu� ma by� utulony podwy�k� ich przydzia�u o t� warto�� 
oraz podwy�k� myszy ekstra o 5. Tygrys ma by� ukarany strat� wspomnianych 10%.
Je�li jednak podwy�ka b�dzie satysfakcjonuj�ca, przydzia� myszy ekstra Tygrysa 
ma wzrosn�� o 5. Zdefiniowa� wyzwalacz (wyzwalacze?), kt�ry b�dzie pe�ni�
funkcj� opisanego wirusa. Poda� przyk�ad jego funkcjonowania a nast�pnie 
zlikwidowa� zmiany.

P.S. Zaproponowa� rozwi�zanie, kt�re ominie podstawowe ograniczenie dla 
wyzwalacza wierszowego aktywowanego poleceniem DML tzn. brak mo�liwo�ci 
odczytu lub zmiany relacji, na kt�rej operacja (polecenie DML) 
�wyzwala� ten wyzwalacz.
*/

SET SERVEROUTPUT ON;

CREATE OR REPLACE PACKAGE z42 AS
PROCEDURE Ustaw(czy_zgoda BOOLEAN, n NUMBER);
PROCEDURE Uwzglednij;
PROCEDURE Zignoruj;
FUNCTION PobierzWartosc RETURN NUMBER;
FUNCTION CzyPasuje RETURN BOOLEAN;
END z42;
/
CREATE OR REPLACE PACKAGE BODY z42 AS
ilosc NUMBER;
czyZgoda BOOLEAN;
czyZignorowac BOOLEAN;

PROCEDURE Ustaw(czy_zgoda BOOLEAN, n NUMBER) IS
BEGIN
czyZgoda:=czy_zgoda;
ilosc:=n;
END;

PROCEDURE Zignoruj IS
BEGIN
czyZignorowac:=true;
END;

PROCEDURE Uwzglednij IS
BEGIN
czyZignorowac:=false;
END;

FUNCTION CzyPasuje RETURN BOOLEAN IS
BEGIN
IF czyZignorowac=TRUE
THEN
RETURN NULL;
ELSE
RETURN czyZgoda;
END IF;
END;

FUNCTION PobierzWartosc RETURN NUMBER IS
BEGIN
RETURN ilosc;
END;
END;
/
CREATE OR REPLACE TRIGGER przedwirus
BEFORE UPDATE OF przydzial_myszy ON Kocury
DECLARE
prz NUMBER;
BEGIN
SELECT przydzial_myszy*0.1 INTO prz
FROM Kocury
WHERE pseudo = 'TYGRYS';
z42.Ustaw(null,prz);
END;
/
CREATE OR REPLACE TRIGGER wirus
AFTER UPDATE OF przydzial_myszy ON Kocury
FOR EACH ROW
DECLARE
staryPrzydzial NUMBER:=:OLD.przydzial_myszy;
nowyPrzydzial NUMBER:=:NEW.przydzial_myszy;
podwyzka NUMBER:=nowyPrzydzial-staryPrzydzial;
procent NUMBER;
BEGIN
procent:=z42.PobierzWartosc;
IF :OLD.funkcja='MILUSIA'
THEN
IF procent > podwyzka
THEN
z42.Ustaw(FALSE,ABS(procent));
ELSE
z42.Ustaw(TRUE,NULL);
END IF;
ELSE
z42.Ustaw(NULL,NULL);
END IF;
END;
/
CREATE OR REPLACE TRIGGER wirus2
AFTER UPDATE OF przydzial_myszy ON Kocury
DECLARE
liczba NUMBER:=z42.PobierzWartosc;
czy_pasuje BOOLEAN:=z42.CzyPasuje;
BEGIN
z42.Ustaw(NULL,NULL);
IF czy_pasuje IS NOT NULL
THEN
IF czy_pasuje THEN
UPDATE Kocury SET myszy_ekstra=myszy_ekstra+5 WHERE pseudo='TYGRYS';
ELSE
UPDATE Kocury
SET przydzial_myszy=przydzial_myszy-liczba
WHERE pseudo='TYGRYS';
z42.Zignoruj;
UPDATE Kocury
SET przydzial_myszy=przydzial_myszy+liczba, myszy_ekstra=myszy_ekstra+5
WHERE funkcja='MILUSIA';
END IF;
END IF;
z42.Uwzglednij;
END;
/
UPDATE Kocury SET przydzial_myszy = 31 WHERE pseudo = 'PUSZYSTA';

SELECT pseudo, przydzial_myszy, myszy_ekstra
FROM Kocury
WHERE funkcja = 'MILUSIA' OR pseudo = 'TYGRYS';

ROLLBACK;


/*Zad. 43. Napisa� blok, kt�ry zrealizuje zad. 33 w spos�b uniwersalny 
(bez konieczno�ci uwzgl�dniania wiedzy o funkcjach pe�nionych przez koty). 
*/
DECLARE
  znaleziono BOOLEAN:=FALSE;
  suma NUMBER:=0;
  totalSuma NUMBER:=0;
  sumaF NUMBER:=0;
  ile NUMBER:=0;
  CURSOR cf IS SELECT funkcja FROM KOCURY;
BEGIN
  DBMS_OUTPUT.PUT(RPAD('NAZWA BANDY',17) || RPAD(' PLEC',7));
  FOR k IN cf
  LOOP
      DBMS_OUTPUT.PUT(LPAD(k.funkcja,12));
  END LOOP;
  DBMS_OUTPUT.PUT_LINE(LPAD('ILE ',7) || LPAD('SUMA',7) || ' ');
  DBMS_OUTPUT.PUT(RPAD('-',17,'-') || ' ' || LPAD('-',7,'-')  || ' ');
  FOR k IN cf
  LOOP
      DBMS_OUTPUT.PUT(RPAD('-',11,'-') || ' ');
  END LOOP;
  DBMS_OUTPUT.PUT(LPAD('-',4,'-') || ' ' || LPAD('-',7,'-'));
  DBMS_OUTPUT.PUT_LINE('');
  
  FOR k IN (SELECT B.nazwa nb, K1.plec pl FROM Kocury K1 INNER JOIN Bandy B ON B.nr_bandy=K1.nr_bandy GROUP BY B.nazwa, K1.plec ORDER BY B.nazwa, K1.plec)
  LOOP
      IF k.pl='D' THEN  
          DBMS_OUTPUT.PUT(RPAD(k.nb || ' ',15));
          DBMS_OUTPUT.PUT(LPAD('Kotka ',9)); 
      ELSE
          DBMS_OUTPUT.PUT(RPAD(' ',15));
          DBMS_OUTPUT.PUT(LPAD('Kocur ',9)); 
      END IF;
      
      FOR f1 IN cf
      LOOP
          znaleziono:=FALSE;
          FOR f2 IN (SELECT funkcja FROM Funkcje)
          LOOP
              IF f2.funkcja=f1.funkcja THEN
                  FOR s IN (SELECT SUM(NVL(K1.przydzial_myszy,0)+NVL(K1.myszy_ekstra,0)) pm, funkcja 
                  FROM Kocury K1 INNER JOIN Bandy B ON B.nr_bandy=K1.nr_bandy 
                  WHERE B.nazwa=k.nb AND K1.plec=k.pl AND K1.funkcja=f2.funkcja 
                  GROUP BY K1.funkcja)
                  LOOP
                      DBMS_OUTPUT.PUT(LPAD(s.pm || ' ',12));
                      znaleziono:=TRUE;
                      suma:=suma+s.pm;
                      ile:=ile+1;
                  END LOOP; 
              END IF;
          END LOOP;
          IF znaleziono=FALSE THEN  DBMS_OUTPUT.PUT(LPAD('0 ',12)); END IF;
      END LOOP;
      
      DBMS_OUTPUT.PUT(LPAD(ile || ' ',7));
      DBMS_OUTPUT.PUT_LINE(LPAD(suma || ' ',7));
      totalSuma:=totalSuma+suma;
      ile:=0;
      suma:=0;
   END LOOP;
   
   DBMS_OUTPUT.PUT(RPAD('-',17,'-') || ' ' || LPAD('-',7,'-')  || ' ');
   FOR k IN cf
   LOOP
        DBMS_OUTPUT.PUT(RPAD('-',11,'-') || ' ');
   END LOOP;
   DBMS_OUTPUT.PUT(LPAD('-',4,'-') || ' ' || LPAD('-',7,'-'));
   DBMS_OUTPUT.PUT_LINE('');
   DBMS_OUTPUT.PUT(RPAD('Suma',24));
   FOR cur in cf
   LOOP
      SELECT SUM(NVL(K.przydzial_myszy,0)+NVL(K.myszy_ekstra,0)) INTO sumaF 
      FROM Kocury K  RIGHT JOIN Funkcje F ON K.funkcja=F.funkcja 
      WHERE cur.funkcja=F.funkcja 
      GROUP BY F.funkcja;
      DBMS_OUTPUT.PUT(LPAD(sumaF || ' ',12));
  END LOOP;
  DBMS_OUTPUT.PUT_LINE(LPAD(totalSuma,13));
END;


/*Zad. 44. Tygrysa zaniepokoi�o niewyt�umaczalne obni�enie zapas�w "myszowych".
Postanowi� wi�c wprowadzi� podatek pog��wny, kt�ry zasili�by spi�arni�. 
Zarz�dzi� wi�c, �e ka�dy kot ma obowi�zek oddawa� 5% (zaokr�glonych w g�r�) 
swoich ca�kowitych "myszowych" przychod�w. Dodatkowo od tego co pozostanie:
-  koty nie posiadaj�ce podw�adnych oddaj� po dwie myszy za nieudolno�� w   
   umizgach o awans,
-  koty nie posiadaj�ce wrog�w oddaj� po jednej myszy za zbytni�  ugodowo��,
    -  koty p�ac� dodatkowy podatek, kt�rego form� okre�la wykonawca zadania.
Napisa� funkcj�, kt�rej parametrem jest pseudonim kota, wyznaczaj�c� nale�ny
podatek pog��wny kota. Funkcj� t� razem z procedur� z zad. 40 nale�y 
umie�ci� w pakiecie, a nast�pnie wykorzysta� j� do okre�lenia podatku 
dla wszystkich kot�w.
*/
CREATE OR REPLACE FUNCTION f44 (m Kocury.pseudo%TYPE, dodatek NUMBER:=0) 
RETURN NUMBER AS podatek NUMBER;
BEGIN
  DECLARE
    ps Kocury.pseudo%TYPE;
    pm NUMBER; -- przydzial myszy
    w NUMBER; --liczba wrog�w
    pw NUMBER; --liczba podwadnych
  BEGIN
    SELECT K.pseudo, MIN(K.przydzial_myszy+NVL(K.myszy_ekstra,0)), COUNT(DISTINCT WK.imie_wroga) 
    INTO ps,pm, w
    FROM Kocury K LEFT JOIN Wrogowie_Kocurow WK ON K.pseudo=WK.pseudo
    WHERE K.pseudo=m
    GROUP BY K.pseudo;
    SELECT COUNT(DISTINCT pseudo) INTO pw FROM Kocury WHERE szef=m; 
    podatek:=CEIL(pm*0.05);
    IF pw<1 THEN podatek:=podatek+2; END IF;
    IF w<1 THEN podatek:=podatek+1; END IF;
    podatek:=podatek+dodatek;
  END;
  RETURN podatek;
END;

select f44('LOLA') from dual;
select f44('LOLA',2) from dual;

CREATE OR REPLACE PACKAGE pakiet44 AS
FUNCTION f44(m Kocury.pseudo%TYPE, dodatek NUMBER:=0) RETURN NUMBER;
PROCEDURE p40(new_nr IN NUMBER, new_nazwa IN VARCHAR2, new_teren IN VARCHAR2);
END pakiet44;

CREATE OR REPLACE PACKAGE BODY pakiet44 AS
FUNCTION f44(m Kocury.pseudo%TYPE, dodatek NUMBER:=0) RETURN NUMBER IS
  podatek NUMBER;
BEGIN
  DECLARE
    ps Kocury.pseudo%TYPE;
    pm NUMBER; -- przydzial myszy
    w NUMBER; --liczba wrog�w
    pw NUMBER; --liczba podwadnych
  BEGIN
    SELECT K.pseudo, MIN(K.przydzial_myszy+NVL(K.myszy_ekstra,0)), COUNT(DISTINCT WK.imie_wroga) 
    INTO ps,pm, w
    FROM Kocury K LEFT JOIN Wrogowie_Kocurow WK ON K.pseudo=WK.pseudo
    WHERE K.pseudo=m
    GROUP BY K.pseudo;
    SELECT COUNT(DISTINCT pseudo) INTO pw FROM Kocury WHERE szef=m; 
    podatek:=CEIL(pm*0.05);
    IF pw<1 THEN podatek:=podatek+2; END IF;
    IF w<1 THEN podatek:=podatek+1; END IF;
    podatek:=podatek+dodatek;
  END;
  RETURN podatek;
END f44;

CREATE OR REPLACE PROCEDURE p40 (new_nr IN NUMBER, new_nazwa IN VARCHAR2, new_teren IN VARCHAR2) 
IS
  exists_val EXCEPTION;
  incorrect EXCEPTION;
BEGIN
  FOR k IN (SELECT nr_bandy, nazwa, teren FROM Bandy)
  LOOP
    IF new_nr<=0 THEN RAISE incorrect; 
    ELSIF  k.nr_bandy=new_nr OR  k.nazwa=new_nazwa OR k.teren=new_teren THEN  RAISE exists_val;
    END IF;
  END LOOP;
  INSERT INTO Bandy(nr_bandy, nazwa, teren) VALUES(new_nr, new_nazwa, new_teren); DBMS_OUTPUT.PUT_LINE('Dodano bande poprawnie.');
EXCEPTION
  WHEN exists_val THEN DBMS_OUTPUT.PUT_LINE('Juz istnieje cz�� wprowadzonych danych. Wprowad� inne dane.');
  WHEN incorrect THEN DBMS_OUTPUT.PUT_LINE('Nr bandy musi by� wi�kszy od 0!');
  WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
END pakiet44;

SELECT pseudo, pakiet44.f44(pseudo) FROM Kocury;
SELECT pseudo, pakiet44.f44(pseudo,2) FROM Kocury;


/*Zad. 45. Tygrys zauwa�y� dziwne zmiany warto�ci swojego prywatnego 
przydzia�u myszy (patrz zadanie 42). Nie niepokoi�y go zmiany na plus 
ale te na minus by�y, jego zdaniem, niedopuszczalne. Zmotywowa� wi�c 
jednego ze swoich szpieg�w do dzia�ania i dzi�ki temu odkry� niecne praktyki
Milu� (zadanie 42). Poleci� wi�c swojemu informatykowi skonstruowanie
mechanizmu zapisuj�cego w relacji Dodatki_ekstra (patrz Wyk�ady - cz. 2) 
dla ka�dej z Milu� -10 (minus dziesi��) myszy dodatku ekstra przy zmianie 
na plus kt�regokolwiek z przydzia��w myszy Milu�, wykonanej przez innego 
operatora ni� on sam. Zaproponowa� taki mechanizm, w zast�pstwie za 
informatyka Tygrysa. W rozwi�zaniu wykorzysta� funkcj� LOGIN_USER zwracaj�c�
nazw� u�ytkownika aktywuj�cego wyzwalacz oraz elementy dynamicznego SQL'a.
*/

-- Zad 45 --
SET SERVEROUTPUT ON;
CREATE TABLE Dodatki_ekstra 
(
pseudo VARCHAR2(15) CONSTRAINT kocury_dodatki REFERENCES Kocury(pseudo), 
dod_ekstra NUMBER(3), CONSTRAINT dodatki PRIMARY KEY(pseudo)
);

CREATE OR REPLACE PACKAGE z45 AS
  PROCEDURE CzyUkarac(czyKara BOOLEAN);
  FUNCTION Ukarac RETURN BOOLEAN; 
  END; 
  / 
 
  CREATE OR REPLACE PACKAGE BODY z45 AS
  kara BOOLEAN; 
   
  PROCEDURE CzyUkarac(czyKara BOOLEAN) IS
    BEGIN 
      kara:=czyKara; 
    END; 
    
  FUNCTION Ukarac 
  RETURN BOOLEAN IS
    BEGIN 
      return kara; 
    END; 
  END;
  / 
  
  CREATE OR REPLACE TRIGGER rozpocznijDzialanie
  AFTER UPDATE OF przydzial_myszy ON Kocury FOR EACH ROW 
  BEGIN
    IF :NEW.funkcja='MILUSIA' AND (:NEW.przydzial_myszy-:OLD.przydzial_myszy) > 0 THEN z45.czyUkarac(true); 
    ELSE z45.czyUkarac(false); 
    END IF;
  END; 
  / 
  
  CREATE OR REPLACE TRIGGER dzialajDalej 
  AFTER UPDATE OF przydzial_myszy ON Kocury 
  DECLARE TYPE pseudon IS TABLE OF VARCHAR2(15) INDEX BY PLS_INTEGER;
  pseudonimy pseudon;
  uzytkownik VARCHAR2(15);
  czyIstnieje NUMBER; 
  BEGIN
    IF z45.Ukarac THEN uzytkownik:=LOGIN_USER; 
    IF uzytkownik <> 'TYGRYS' THEN
      EXECUTE IMMEDIATE 'SELECT pseudo FROM Kocury WHERE funkcja = ''MILUSIA'''  BULK COLLECT INTO pseudonimy;
      FOR i IN 1..pseudonimy.
        COUNT 
          LOOP 
            SELECT COUNT(*) INTO czyIstnieje
            FROM Dodatki_ekstra 
            WHERE pseudo = pseudonimy(i); 
          IF czyIstnieje < 1 THEN EXECUTE IMMEDIATE 'INSERT INTO Dodatki_ekstra VALUES(''' || pseudonimy(i) || ''',-10)'; 
          ELSE EXECUTE IMMEDIATE 'UPDATE Dodatki_ekstra SET dod_ekstra = dod_ekstra-10 WHERE pseudo = '' ' || pseudonimy(i)||' '' '; 
          END IF;
        END LOOP;
      END IF; 
    END IF;
    z45.czyUkarac(FALSE);
  END; 
  / 
  
UPDATE Kocury SET przydzial_myszy = 21 WHERE pseudo = 'PUSZYSTA';
SELECT * FROM Dodatki_ekstra;

ROLLBACK;