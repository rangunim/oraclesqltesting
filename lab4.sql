--(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=ORACLE.win.ii.pwr.wroc.pl)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)SERVICE_NAME=orcl.win.ii.pwr.wroc.pl)))
ALTER SESSION SET NLS_Date_Format = 'YYYY-MM-DD';

/*Zad. 46. Za�o�y�, �e w stadzie kot�w pojawi� si� podzia� na elit� i na plebs. 
Cz�onek elity posiada� prawo do jednego s�ugi wybranego spo�r�d plebsu. 
Dodatkowo m�g� gromadzi� myszy na dost�pnym dla ka�dego cz�onka elity koncie. 
Konto ma zawiera� dane o dacie wprowadzenia na nie pojedynczej myszy i o dacie 
jej usuni�cia. O tym, do kogo nale�y mysz ma m�wi� odniesienie do jej 
w�a�ciciela z elity. Przyjmuj�c te dodatkowe za�o�enia zdefiniowa� schemat 
bazy danych kot�w w postaci relacyjno-obiektowej, gdzie dane dotycz�ce kot�w, 
elity, plebsu. kont, funkcji, band, wrog�w, incydent�w b�d� okre�lane przez 
odpowiednie typy obiektowe. Dla ka�dego z typ�w zaproponowa� i zdefiniowa�
przyk�adowe metody. Powi�zania referencyjne nale�y zdefiniowa� za pomoc� typ�w 
odniesienia. Tak przygotowany schemat wype�ni� danymi z rzeczywisto�ci kot�w 
(dane do opisu elit, plebsu i kont zaproponowa� samodzielnie) a nast�pnie
wykona� przyk�adowe zapytania SQL wykorzystuj�ce z��czenia, podzapytania i 
grupowanie, ilustruj�ce przy okazji zastosowanie zdefiniowanych metod.
Zrealizowa� dodatkowo w ramach nowego, relacyjno-obiektowego, schematu kilka 
wybranych zada� z list nr  2 i 3.
*/




CREATE OR REPLACE TYPE BANDYOb AS OBJECT
( 
  nr_bandy NUMBER(2), 
  nazwa VARCHAR2(20), 
  teren VARCHAR(15), 
  MAP MEMBER FUNCTION equals RETURN VARCHAR2, 
  MEMBER FUNCTION toString RETURN VARCHAR2 
); 
/ 
--------------------------------------------------------------------------------
CREATE OR REPLACE TYPE FUNKCJEOb AS OBJECT
( 
  funkcja VARCHAR2(10), 
  min_myszy NUMBER(3), 
  max_myszy NUMBER(3), 
  MAP MEMBER FUNCTION equals RETURN VARCHAR2, 
  MEMBER FUNCTION toString RETURN VARCHAR2 
); 
/ 
--------------------------------------------------------------------------------
CREATE OR REPLACE TYPE WROGOWIEOb AS OBJECT
( 
  imie_wroga VARCHAR2(15), 
  stopien_wrogosci NUMBER(2), 
  gatunek VARCHAR2(15), 
  lapowka VARCHAR2(20), 
  MAP MEMBER FUNCTION equals RETURN VARCHAR2, 
  MEMBER FUNCTION toString RETURN VARCHAR2 
);
/ 
--------------------------------------------------------------------------------
CREATE OR REPLACE TYPE KOCURYOb AS OBJECT
( 
  imie VARCHAR2(15), 
  plec VARCHAR2(1), 
  pseudo VARCHAR2(15), 
  funkcja REF FUNKCJEOb,
  szef REF KOCURYOb,   
  w_stadku_od DATE,
  przydzial_myszy NUMBER(3),
  myszy_ekstra NUMBER(3),
  nr_bandy REF BANDYOb,
  MAP MEMBER FUNCTION equals RETURN VARCHAR2, 
  MEMBER FUNCTION toString RETURN VARCHAR2,  
  MEMBER FUNCTION getFunkcja RETURN VARCHAR2,
  MEMBER FUNCTION getSzef RETURN VARCHAR2,
  MEMBER FUNCTION getNrBandy RETURN VARCHAR2
) NOT FINAL;
/ 
--------------------------------------------------------------------------------
CREATE OR REPLACE TYPE WROGOWIE_KOCUROWOb AS OBJECT
( 
    nr_incydentu NUMBER, 
    pseudo REF KOCURYOb, 
    imie_wroga REF WROGOWIEOb, 
    data_incydentu DATE, 
    opis_incydentu VARCHAR2(50),
    MAP MEMBER FUNCTION equals RETURN VARCHAR2, 
    MEMBER FUNCTION toString RETURN VARCHAR2,
    MEMBER FUNCTION getPseudo RETURN VARCHAR2, 
    MEMBER FUNCTION getImieWroga RETURN VARCHAR2
); 
/ 
--------------------------------------------------------------------------------
CREATE OR REPLACE TYPE ElitaOb UNDER KocuryOb
(sluga REF KocuryOb);

CREATE OR REPLACE TYPE PlebsOb UNDER KocuryOb
(pan REF KocuryOb);

CREATE OR REPLACE TYPE KONTAOb AS OBJECT
( 
    k_id NUMBER, 
    wlasciciel REF ELITAOb, 
    wprowadzenie_myszy DATE, 
    usuniecie_myszy DATE, 
    MAP MEMBER FUNCTION equals RETURN VARCHAR2,
    MEMBER FUNCTION toString RETURN VARCHAR2,
    MEMBER FUNCTION getWlasciciel RETURN VARCHAR2
);
/ 

--------------------------------------------------------------------------------
---------------------------dodatkowe metody-------------------------------------
ALTER TYPE BandyOb
ADD MEMBER FUNCTION srednia RETURN NUMBER,
ADD MEMBER FUNCTION minim RETURN NUMBER,
ADD MEMBER FUNCTION maxim RETURN NUMBER
CASCADE;

ALTER TYPE FunkcjeOb 
ADD MEMBER FUNCTION ile_dniMaxMin RETURN NUMBER
CASCADE;

ALTER TYPE KocuryOb
ADD MEMBER FUNCTION calkowityPrzydzial RETURN NUMBER 
CASCADE;
--------------------------------------------------------------------------------
-------------------------------wypelnanie metod --------------------------------
CREATE OR REPLACE TYPE BODY BANDYOb AS 
MAP MEMBER FUNCTION equals RETURN VARCHAR2 IS 
BEGIN 
    RETURN nr_bandy || nazwa || teren; 
END; 

MEMBER FUNCTION toString RETURN VARCHAR2 IS 
BEGIN 
  RETURN nr_bandy || ', ' || nazwa || ', ' || teren; 
END;

MEMBER FUNCTION srednia RETURN NUMBER IS sred number;
BEGIN
  SELECT avg(K.calkowityPrzydzial()) INTO sred
  FROM KocuryT K
  WHERE DEREF(K.nr_bandy) = self;
  RETURN sred;
END;

MEMBER FUNCTION minim RETURN NUMBER IS wartMin number;
BEGIN
  SELECT min(K.calkowityPrzydzial()) INTO wartMin
  FROM KocuryT K
  WHERE DEREF(K.nr_bandy) = self;
  RETURN wartMin;
END;
  
MEMBER FUNCTION maxim RETURN NUMBER IS wartMax number;
BEGIN
  SELECT max(K.calkowityPrzydzial()) INTO wartMax
  FROM KocuryT K
  WHERE DEREF(K.nr_bandy) = self;
  RETURN wartMax;
END;
END;
/
--------------------------------------------------------------------------------

CREATE OR REPLACE TYPE BODY FUNKCJEOb AS 
MAP MEMBER FUNCTION equals RETURN VARCHAR2 IS 
BEGIN 
  RETURN funkcja || min_myszy || max_myszy; 
END; 

MEMBER FUNCTION toString RETURN VARCHAR2 IS 
BEGIN 
  RETURN funkcja || ', ' || min_myszy || ', ' || max_myszy; 
END; 

MEMBER FUNCTION ile_dniMaxMin RETURN NUMBER IS 
BEGIN 
  RETURN max_myszy-min_myszy; 
END; 
END; 
/ 
--------------------------------------------------------------------------------
CREATE OR REPLACE TYPE BODY WROGOWIEOb AS 
MAP MEMBER FUNCTION equals RETURN VARCHAR2 IS 
BEGIN 
    RETURN imie_wroga || stopien_wrogosci || gatunek || lapowka;
END; 

MEMBER FUNCTION toString RETURN VARCHAR2 IS
BEGIN 
  RETURN imie_wroga || ', ' || stopien_wrogosci || ', ' || gatunek || ', ' || lapowka; 
END; 
END;
/ 
--------------------------------------------------------------------------------

CREATE OR REPLACE TYPE BODY KOCURYOb AS 
MAP MEMBER FUNCTION equals RETURN VARCHAR2 IS 
BEGIN 
    RETURN imie || plec || pseudo || getFunkcja() || getSzef() || w_stadku_od 
           || przydzial_myszy || myszy_ekstra || getNrBandy(); 
END; 

MEMBER FUNCTION getFunkcja RETURN VARCHAR2 AS f FUNKCJEOb; 
BEGIN 
    SELECT DEREF(funkcja) INTO f FROM dual; 
    RETURN f.funkcja; 
END; 
    
MEMBER FUNCTION getSzef RETURN VARCHAR2 AS s KOCURYOb; 
BEGIN 
    SELECT DEREF(szef) INTO s FROM dual; 
    RETURN s.pseudo; 
END; 

MEMBER FUNCTION getNrBandy RETURN VARCHAR2 AS b BANDYOb; 
BEGIN 
    SELECT DEREF(nr_bandy) INTO b FROM dual; 
    RETURN b.nr_bandy; 
END; 
    
MEMBER FUNCTION toString RETURN VARCHAR2 IS
BEGIN 
    RETURN imie || ', ' || pseudo || ', ' || plec || ', ' || getFunkcja() 
           || ', ' || getSzef() || ', ' || w_stadku_od || ', ' || 
           przydzial_myszy || ', ' || myszy_ekstra || ', ' || getNrBandy(); 
END; 

MEMBER FUNCTION calkowityPrzydzial RETURN NUMBER IS 
BEGIN 
    RETURN NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0); 
END;
END;   
/ 
--------------------------------------------------------------------------------

CREATE OR REPLACE TYPE BODY WROGOWIE_KOCUROWOb AS 
MAP MEMBER FUNCTION equals RETURN VARCHAR2 IS 
BEGIN 
  RETURN getPseudo() || getImieWroga() || data_incydentu || opis_incydentu; 
END; 

MEMBER FUNCTION getPseudo RETURN VARCHAR2 AS k KOCURYOb; 
BEGIN 
    SELECT DEREF(pseudo) INTO k FROM dual; 
    RETURN k.pseudo; 
END; 

MEMBER FUNCTION getImieWroga RETURN VARCHAR2 AS w WROGOWIEOb; 
BEGIN 
    SELECT DEREF(imie_wroga) INTO w FROM dual; 
    RETURN w.imie_wroga; 
END; 

MEMBER FUNCTION toString RETURN VARCHAR2 IS 
BEGIN 
    RETURN getPseudo() || ', ' || getImieWroga() || ', ' || data_incydentu || ', '
          || opis_incydentu;
END; 
END; 
/ 

--------------------------------------------------------------------------------
---------------------tworzenie tabel--------------------------------------------
CREATE TABLE BandyT OF BandyOb
(nr_bandy CONSTRAINT pk_bnd_ob PRIMARY KEY);

CREATE TABLE FunkcjeT OF FunkcjeOb
(funkcja CONSTRAINT pk_fun_ob PRIMARY KEY,
min_myszy CONSTRAINT min_wart CHECK (min_myszy>5),
max_myszy CONSTRAINT max_wart CHECK (max_myszy<200),
CONSTRAINT max_min CHECK (max_myszy>=min_myszy));

CREATE TABLE WrogowieT OF WrogowieOB
(imie_wroga CONSTRAINT pk_im_wr PRIMARY KEY,
stopien_wrogosci CONSTRAINT stop_wart CHECK (stopien_wrogosci>=1 AND stopien_wrogosci<=10));

CREATE TABLE KocuryT OF KocuryOb
(imie NOT NULL,
plec CONSTRAINT plec_ch CHECK (plec IN ('D','M')),
pseudo CONSTRAINT pk_koc PRIMARY KEY,
w_stadku_od DEFAULT SYSDATE,
szef SCOPE IS KocuryT,
funkcja SCOPE IS FunkcjeT,
nr_bandy SCOPE IS BandyT);

CREATE TABLE Wrogowie_KocurowT OF Wrogowie_KocurowOb
(nr_incydentu CONSTRAINT pk_wr_koc PRIMARY KEY,
pseudo SCOPE IS KocuryT,
imie_wroga SCOPE IS WrogowieT,
data_incydentu NOT NULL);

CREATE TABLE ElitaT OF ElitaOb
(sluga SCOPE IS KocuryT);

CREATE TABLE PlebsT OF PlebsOb
(pan SCOPE IS KocuryT);

CREATE TABLE KontoT
(id_m NUMBER(4) CONSTRAINT pk_kon PRIMARY KEY,
wlasciciel REF ElitaOb SCOPE IS ElitaT,
data_dod DATE NOT NULL,
data_usun DATE, 
CONSTRAINT date_ch CHECK (nvl(data_usun,data_dod)>=data_dod));


--------------------------------------------------------------------------------
---------------------wypelnianie danymi-----------------------------------------
INSERT ALL
INTO BandyT VALUES (1,'SZEFOSTWO','CALOSC')
INTO BandyT VALUES (2,'CZARNI RYCERZE','POLE')
INTO BandyT VALUES (3,'BIALI LOWCY','SAD')
INTO BandyT VALUES (4,'LACIACI MYSLIWI','GORKA')
INTO BandyT VALUES (5,'ROCKERSI','ZAGRODA')
SELECT * FROM DUAL;

INSERT ALL
INTO FunkcjeT VALUES ('SZEFUNIO',90,110)
INTO FunkcjeT VALUES ('BANDZIOR',70,90)
INTO FunkcjeT VALUES ('LOWCZY',60,70)
INTO FunkcjeT VALUES ('LAPACZ',50,60)
INTO FunkcjeT VALUES ('KOT',40,50)
INTO FunkcjeT VALUES ('MILUSIA',20,30)
INTO FunkcjeT VALUES ('DZIELCZY',45,55)
INTO FunkcjeT VALUES ('HONOROWA',6,25)
SELECT * FROM DUAL;

INSERT ALL
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('KAZIO',10,'CZLOWIEK','FLASZKA')
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('GLUPIA ZOSKA',1,'CZLOWIEK','KORALIK')
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('SWAWOLNY DYZIO',7,'CZLOWIEK','GUMA DO ZUCIA')
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('BUREK',4,'PIES','KOSC')
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('DZIKI BILL',10,'PIES',NULL)
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('REKSIO',2,'PIES','KOSC')
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('BETHOVEN',1,'PIES','PEDIGRIPALL')
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('CHYTRUSEK',5,'LIS','KURCZAK')
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('SMUKLA',1,'SOSNA',NULL)
INTO WrogowieT(imie_wroga,stopien_wrogosci,gatunek,lapowka) VALUES ('BAZYLI',3,'KOGUT','KURA DO STADA')
SELECT * FROM DUAL;

INSERT INTO KocuryT Values('MRUCZEK','M','TYGRYS',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='SZEFUNIO'),NULL,'2002-01-01',103,33,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 1));
INSERT INTO KocuryT Values('MICKA','D','LOLA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='MILUSIA'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2009-10-14',25,47,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 1));
INSERT INTO KocuryT Values('CHYTRY','M','BOLEK',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='DZIELCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2002-05-05',50,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 1));
INSERT INTO KocuryT Values('KOREK','M','ZOMBI',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='BANDZIOR'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2004-03-16',75,13,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 3));
INSERT INTO KocuryT Values('BOLEK','M','LYSY',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='BANDZIOR'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2006-08-15',72,21,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2));
INSERT INTO KocuryT Values('RUDA','D','MALA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='MILUSIA'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2006-09-17',22,42,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 1));
INSERT INTO KocuryT Values('PUCEK','M','RAFA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LOWCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2006-10-15',65,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4));
INSERT INTO KocuryT Values('JACEK','M','PLACEK',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja = 'LOWCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'),'2008-12-01',67,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2));
INSERT INTO KocuryT Values('BARI','M','RURA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LAPACZ'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'),'2009-09-01',56,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2));
INSERT INTO KocuryT Values('ZUZIA','D','SZYBKA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LOWCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'),'2006-07-21',65,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2));
INSERT INTO KocuryT Values('BELA','D','LASKA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='MILUSIA'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'),'2008-02-01',24,28,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2));
INSERT INTO KocuryT Values('PUNIA','D','KURKA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LOWCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'ZOMBI'),'2008-01-01',61,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 3));
INSERT INTO KocuryT Values('SONIA','D','PUSZYSTA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='MILUSIA'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'ZOMBI'),'2010-11-18',20,35,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 3));
INSERT INTO KocuryT Values('LUCEK','M','ZERO',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='KOT'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'KURKA'),'2010-03-01',43,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 3));
INSERT INTO KocuryT Values('LATKA','D','UCHO',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='KOT'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'RAFA'),'2011-01-01',40,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4));
INSERT INTO KocuryT Values('DUDEK','M','MALY',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='KOT'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'RAFA'),'2011-05-15',40,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4));
INSERT INTO KocuryT Values('KSAWERY','M','MAN',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LAPACZ'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'RAFA'),'2008-07-12',51,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4));
INSERT INTO KocuryT Values('MELA','D','DAMA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LAPACZ'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'RAFA'),'2008-11-01',51,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4));

INSERT ALL
INTO Wrogowie_kocurowT VALUES (1,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='TYGRYS'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='KAZIO'),'2004-10-13','USILOWAL NABIC NA WIDLY')
INTO Wrogowie_kocurowT VALUES (2,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='ZOMBI'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='SWAWOLNY DYZIO'),'2005-03-07','WYBIL OKO Z PROCY')
INTO Wrogowie_kocurowT VALUES (3,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='BOLEK'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='KAZIO'),'2005-03-29','POSZCZUL BURKIEM')
INTO Wrogowie_kocurowT VALUES (4,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='SZYBKA'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='GLUPIA ZOSKA'),'2006-09-12','UZYLA KOTA JAKO SCIERKI')
INTO Wrogowie_kocurowT VALUES (5,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='MALA'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='CHYTRUSEK'),'2007-03-07','ZALECAL SIE')
INTO Wrogowie_kocurowT VALUES (6,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='TYGRYS'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='DZIKI BILL'),'2007-06-12','USILOWAL POZBAWIC ZYCIA')
INTO Wrogowie_kocurowT VALUES (7,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='BOLEK'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='DZIKI BILL'),'2007-11-10','ODGRYZL UCHO')
INTO Wrogowie_kocurowT VALUES (8,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='LASKA'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='DZIKI BILL'),'2008-12-12','POGRYZL ZE LEDWO SIE WYLIZALA')
INTO Wrogowie_kocurowT VALUES (9,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='LASKA'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='KAZIO'),'2009-01-07','ZLAPAL ZA OGON I ZROBIL WIATRAK')
INTO Wrogowie_kocurowT VALUES (10,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='DAMA'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='KAZIO'),'2009-02-07','CHCIAL OBEDRZEC ZE SKORY')
INTO Wrogowie_kocurowT VALUES (11,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='MAN'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='REKSIO'),'2009-04-14','WYJATKOWO NIEGRZECZNIE OBSZCZEKAL')
INTO Wrogowie_kocurowT VALUES (12,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='LYSY'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='BETHOVEN'),'2009-05-11','NIE PODZIELIL SIE SWOJA KASZA')
INTO Wrogowie_kocurowT VALUES (13,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='RURA'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='DZIKI BILL'),'2009-09-03','ODGRYZL OGON')
INTO Wrogowie_kocurowT VALUES (14,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='PLACEK'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='BAZYLI'),'2010-07-12','DZIOBIAC UNIEMOZLIWIL PODEBRANIE KURCZAKA')
INTO Wrogowie_kocurowT VALUES (15,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='PUSZYSTA'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='SMUKLA'),'2010-11-19','OBRZUCILA SZYSZKAMI')
INTO Wrogowie_kocurowT VALUES (16,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='KURKA'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='BUREK'),'2010-12-14','POGONIL')
INTO Wrogowie_kocurowT VALUES (17,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='MALY'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='CHYTRUSEK'),'2011-07-13','PODEBRAL PODEBRANE JAJKA')
INTO Wrogowie_kocurowT VALUES (18,(SELECT REF(K) FROM KocuryT K WHERE K.pseudo ='UCHO'),(SELECT REF(w) FROM wrogowieT W WHERE W.imie_wroga ='SWAWOLNY DYZIO'),'2011-07-14','OBRZUCIL KAMIENIAMI')
SELECT * FROM DUAL;


INSERT INTO ElitaT Values('MRUCZEK','M','TYGRYS',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='SZEFUNIO'),NULL,'2002-01-01',103,33,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 1), (SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'ZERO'));
INSERT INTO ElitaT Values('MICKA','D','LOLA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='MILUSIA'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2009-10-14',25,47,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 1),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LASKA'));
INSERT INTO ElitaT Values('KOREK','M','ZOMBI',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='BANDZIOR'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2004-03-16',75,13,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 3),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'MAN'));
INSERT INTO ElitaT Values('BOLEK','M','LYSY',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='BANDZIOR'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2006-08-15',72,21,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'BOLEK'));
INSERT INTO ElitaT Values('JACEK','M','PLACEK',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja = 'LOWCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'),'2008-12-01',67,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'MALY'));

INSERT INTO PlebsT Values('CHYTRY','M','BOLEK',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='DZIELCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2002-05-05',50,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 1),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'));
INSERT INTO PlebsT Values('RUDA','D','MALA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='MILUSIA'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2006-09-17',22,42,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 1),NULL);
INSERT INTO PlebsT Values('PUCEK','M','RAFA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LOWCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'),'2006-10-15',65,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4),NULL);
INSERT INTO PlebsT Values('BARI','M','RURA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LAPACZ'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'),'2009-09-01',56,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2),NULL);
INSERT INTO PlebsT Values('ZUZIA','D','SZYBKA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LOWCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'),'2006-07-21',65,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2),NULL);
INSERT INTO PlebsT Values('BELA','D','LASKA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='MILUSIA'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LYSY'),'2008-02-01',24,28,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 2),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'LOLA'));
INSERT INTO PlebsT Values('PUNIA','D','KURKA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LOWCZY'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'ZOMBI'),'2008-01-01',61,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 3),NULL);
INSERT INTO PlebsT Values('SONIA','D','PUSZYSTA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='MILUSIA'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'ZOMBI'),'2010-11-18',20,35,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 3),NULL);
INSERT INTO PlebsT Values('LUCEK','M','ZERO',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='KOT'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'KURKA'),'2010-03-01',43,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 3),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'TYGRYS'));
INSERT INTO PlebsT Values('LATKA','D','UCHO',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='KOT'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'RAFA'),'2011-01-01',40,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4),NULL);
INSERT INTO PlebsT Values('DUDEK','M','MALY',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='KOT'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'RAFA'),'2011-05-15',40,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'PLACEK'));
INSERT INTO PlebsT Values('KSAWERY','M','MAN',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LAPACZ'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'RAFA'),'2008-07-12',51,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'ZOMBI'));
INSERT INTO PlebsT Values('MELA','D','DAMA',(SELECT REF(F) FROM FunkcjeT F WHERE F.funkcja ='LAPACZ'),(SELECT REF(K) FROM KocuryT K WHERE K.pseudo = 'RAFA'),'2008-11-01',51,NULL,(SELECT REF(B) FROM BandyT B WHERE B.nr_bandy = 4),NULL);

INSERT INTO KontoT VALUES (1,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'TYGRYS'),'2012-01-01','2012-11-11');
INSERT INTO KontoT VALUES (2,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'TYGRYS'),'2012-03-04','2013-10-11');
INSERT INTO KontoT VALUES (3,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'LOLA'),'2012-11-11','2012-12-12');
INSERT INTO KontoT VALUES (4,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'LOLA'),'2013-05-08','2014-11-11');
INSERT INTO KontoT VALUES (5,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'TYGRYS'),'2013-06-09',NULL);
INSERT INTO KontoT VALUES (6,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'ZOMBI'),'2013-10-01',NULL);
INSERT INTO KontoT VALUES (7,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'LYSY'),'2013-10-11','2014-01-11');
INSERT INTO KontoT VALUES (8,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'TYGRYS'),'2013-11-11','2014-02-01');
INSERT INTO KontoT VALUES (9,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'ZOMBI'),'2013-11-12','2014-03-01');
INSERT INTO KontoT VALUES (10,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'LYSY'),'2014-01-01','2014-04-04');
INSERT INTO KontoT VALUES (11,(SELECT REF(E) FROM ElitaT E WHERE E.pseudo = 'TYGRYS'),'2014-01-02',NULL);





/*Zad. 47. Rozszerzy� relacyjn� baz� danych kot�w o dodatkowe relacje opisuj�ce 
elit�, plebs i konta elity (patrz opis z zad. 46) a nast�pnie zdefiniowa� 
"nak�adk�", w postaci perspektyw obiektowych, na tak zmodyfikowan� baz�. 
Odpowiadaj�ce relacjom typy obiektowe maj� zawiera� przyk�adowe metody 
(mog� to by� metody z zad. 46). Zamodelowa� wszystkie powi�zania referencyjne 
z wykorzystaniem identyfikator�w OID i funkcji MAKE_REF. Relacje wype�ni� 
przyk�adowymi danymi (mog� to by� dane z zad. 46). Dla tak przygotowanej 
bazy wykona� wszystkie zapytania SQL i bloki PL/SQL zrealizowane 
w ramach zad. 46.*/

CREATE OR REPLACE TYPE BandyTyp AS OBJECT
(nr_bandy NUMBER(2),
nazwa VARCHAR(20),
teren VARCHAR(15),
MEMBER FUNCTION Srednia RETURN NUMBER,
MEMBER FUNCTION Minim RETURN NUMBER,
MEMBER FUNCTION Maxim RETURN NUMBER);

CREATE OR REPLACE TYPE FunkcjeTyp AS OBJECT
(funkcja VARCHAR2(10),
min_myszy NUMBER(3),
max_myszy NUMBER(3),
MEMBER FUNCTION Srednia RETURN NUMBER,
MEMBER FUNCTION Minim RETURN NUMBER,
MEMBER FUNCTION Maxim RETURN NUMBER);

CREATE OR REPLACE TYPE WrogowieTyp AS OBJECT
(imie_wroga VARCHAR2(15),
stopien_wrogosci NUMBER(2),
gatunek VARCHAR2(15),
lapowka VARCHAR2(20),
MEMBER FUNCTION IleKotow RETURN NUMBER,
MEMBER FUNCTION KiedyOstatni RETURN DATE);

CREATE OR REPLACE TYPE KocuryTyp AS OBJECT
(imie     VARCHAR2(15),
plec    VARCHAR2(1),
pseudo  VARCHAR2(15),
funkcja REF FunkcjeTyp,
szef    VARCHAR2(15),
w_stadku_od DATE,
przydzial_myszy NUMBER(3),
myszy_ekstra NUMBER(3),
nr_bandy REF BandyTyp,
MEMBER FUNCTION CalkowityPrzydzial RETURN NUMBER,
MEMBER FUNCTION Staz RETURN INTEGER);

CREATE OR REPLACE TYPE Wrogowie_KocurowTyp AS OBJECT
(pseudo VARCHAR2(15),
imie_wroga VARCHAR2(15),
data_incydentu DATE,
opis_incydentu VARCHAR2(50),
MEMBER FUNCTION DajPowod RETURN VARCHAR2);

CREATE TABLE Plebs
(pl_pseudo VARCHAR2(15) CONSTRAINT pk_pl_tab PRIMARY KEY,
CONSTRAINT fk_pl_ps FOREIGN KEY (pl_pseudo) REFERENCES Kocury(pseudo));

CREATE TABLE Elita
(el_pseudo VARCHAR2(15) CONSTRAINT pk_el_tab PRIMARY KEY,
sluga  VARCHAR2(15),
CONSTRAINT fk_el_ps FOREIGN KEY (el_pseudo) REFERENCES Kocury(pseudo),
CONSTRAINT fk_el_sl FOREIGN KEY (sluga) REFERENCES Plebs(pl_pseudo));

CREATE TABLE Konto
(id_m NUMBER(4) CONSTRAINT pk_kon_t PRIMARY KEY,
wlasciciel VARCHAR2(15),
data_dod DATE NOT NULL,
data_usun DATE, 
CONSTRAINT fk_wl_ps FOREIGN KEY (wlasciciel) REFERENCES Elita(el_pseudo),
CONSTRAINT date_ch CHECK (nvl(data_usun,data_dod)>=data_dod));

INSERT INTO Plebs VALUES ('BOLEK');
INSERT INTO Plebs VALUES ('MALA');
INSERT INTO Plebs VALUES ('RAFA');
INSERT INTO Plebs VALUES ('RURA');
INSERT INTO Plebs VALUES ('SZYBKA');
INSERT INTO Plebs VALUES ('LASKA');
INSERT INTO Plebs VALUES ('KURKA');
INSERT INTO Plebs VALUES ('PUSZYSTA');
INSERT INTO Plebs VALUES ('ZERO');
INSERT INTO Plebs VALUES ('UCHO');
INSERT INTO Plebs VALUES ('MALY');
INSERT INTO Plebs VALUES ('MAN');
INSERT INTO Plebs VALUES ('DAMA');

INSERT INTO Elita VALUES ('ZOMBI','MAN');
INSERT INTO Elita VALUES ('TYGRYS','ZERO');
INSERT INTO Elita VALUES ('LOLA','LASKA');
INSERT INTO Elita VALUES ('LYSY','BOLEK');
INSERT INTO Elita VALUES ('PLACEK','MALY');

INSERT INTO Konto VALUES (1,'TYGRYS','2012-01-01','2012-11-11');
INSERT INTO Konto VALUES (2,'TYGRYS','2012-03-04','2013-10-11');
INSERT INTO Konto VALUES (3,'LOLA','2012-11-11','2012-12-12');
INSERT INTO Konto VALUES (4,'LOLA','2013-05-08','2014-11-11');
INSERT INTO Konto VALUES (5,'TYGRYS','2013-06-09',NULL);
INSERT INTO Konto VALUES (6,'ZOMBI','2013-10-01',NULL);
INSERT INTO Konto VALUES (7,'LYSY','2013-10-11','2014-01-11');
INSERT INTO Konto VALUES (8,'TYGRYS','2013-11-11','2014-02-01');
INSERT INTO Konto VALUES (9,'ZOMBI','2013-11-12','2014-03-01');
INSERT INTO Konto VALUES (10,'LYSY','2014-01-01','2014-04-04');
INSERT INTO Konto VALUES (11,'TYGRYS','2014-01-02',NULL);

CREATE OR REPLACE TYPE PlebsTyp AS OBJECT
(pl_pseudo VARCHAR2(15),
pl_pseudo_ref REF KocuryTyp);

CREATE OR REPLACE TYPE ElitaTyp AS OBJECT
(el_pseudo VARCHAR2(15),
el_pseudo_ref REF KocuryTyp,
sluga REF PlebsTyp,
MEMBER FUNCTION StanKonta RETURN NUMBER);

CREATE OR REPLACE TYPE KontoTyp AS OBJECT
(id_m NUMBER(4),
wlasciciel REF ElitaTyp,
data_dod DATE,
data_usun DATE,
MEMBER FUNCTION Dlugosc RETURN NUMBER);



----------------perspektywy-----------------------------------------------------
CREATE OR REPLACE VIEW Bandy_V OF BandyTyp
WITH OBJECT IDENTIFIER (nr_bandy) AS
SELECT nr_bandy, nazwa, teren
FROM Bandy;

CREATE OR REPLACE VIEW Funkcje_V OF FunkcjeTyp
WITH OBJECT IDENTIFIER (funkcja) AS
SELECT funkcja, min_myszy, max_myszy
FROM Funkcje;

CREATE OR REPLACE VIEW Wrogowie_V OF WrogowieTyp
WITH OBJECT IDENTIFIER (imie_wroga) AS
SELECT imie_wroga, stopien_wrogosci, gatunek, lapowka
FROM Wrogowie;

CREATE OR REPLACE VIEW Kocury_V OF KocuryTyp
WITH OBJECT IDENTIFIER (pseudo) AS
SELECT imie, plec, pseudo, MAKE_REF(Funkcje_V, funkcja) funkcja, szef, w_stadku_od,
    przydzial_myszy, myszy_ekstra, MAKE_REF(Bandy_V, nr_bandy) nr_bandy
FROM Kocury;

CREATE OR REPLACE VIEW Wrogowie_Kocurow_V OF Wrogowie_KocurowTyp
WITH OBJECT IDENTIFIER (pseudo, imie_wroga) AS
SELECT pseudo, imie_wroga, data_incydentu, opis_incydentu
FROM Wrogowie_Kocurow;

CREATE OR REPLACE VIEW Plebs_V OF PlebsTyp
WITH OBJECT IDENTIFIER (pl_pseudo) AS
SELECT pl_pseudo, MAKE_REF(Kocury_V,pl_pseudo) pl_pseudo_ref
FROM Plebs;

CREATE OR REPLACE VIEW Elita_V OF ElitaTyp
WITH OBJECT IDENTIFIER (el_pseudo) AS
SELECT el_pseudo, MAKE_REF(Kocury_V,el_pseudo) el_pseudo_ref, MAKE_REF(Plebs_V,sluga) sluga
FROM Elita;

CREATE OR REPLACE VIEW KontaV OF KontoTyp
WITH OBJECT IDENTIFIER (id_m) AS
SELECT id_m, MAKE_REF(Elita_V, wlasciciel) wlasciciel, data_dod, data_usun
FROM Konto;


--------------wypelnainie metod----------------------------------------------
CREATE OR REPLACE TYPE BODY BandyTyp AS
  MEMBER FUNCTION Srednia RETURN NUMBER IS
  sred number;
  BEGIN
    SELECT avg(K.CalkowityPrzydzial())
    INTO sred
    FROM Kocury_V K
    WHERE DEREF(K.nr_bandy) = self;
    RETURN sred;
  END;
  MEMBER FUNCTION Minim RETURN NUMBER IS
  wartMin number;
  BEGIN
    SELECT min(K.CalkowityPrzydzial())
    INTO wartMin
    FROM Kocury_V K
    WHERE DEREF(K.nr_bandy) = self;
    RETURN wartMin;
  END;
  MEMBER FUNCTION Maxim RETURN NUMBER IS
  wartMax number;
  BEGIN
    SELECT max(K.CalkowityPrzydzial())
    INTO wartMax
    FROM Kocury_V K
    WHERE DEREF(K.nr_bandy) = self;
    RETURN wartMax;
  END;
END;

CREATE OR REPLACE TYPE BODY FunkcjeTyp AS
  MEMBER FUNCTION Srednia RETURN NUMBER IS
  sred number;
  BEGIN
    SELECT avg(K.CalkowityPrzydzial())
    INTO sred
    FROM Kocury_V K
    WHERE DEREF(K.funkcja) = self;
    RETURN sred;
  END;
  MEMBER FUNCTION Minim RETURN NUMBER IS
  wartMin number;
  BEGIN
    SELECT min(K.CalkowityPrzydzial())
    INTO wartMin
    FROM Kocury_V K
    WHERE DEREF(K.funkcja) = self;
    RETURN wartMin;
  END;
  MEMBER FUNCTION Maxim RETURN NUMBER IS
  wartMax number;
  BEGIN
    SELECT max(K.CalkowityPrzydzial())
    INTO wartMax
    FROM Kocury_V K
    WHERE DEREF(K.funkcja) = self;
    RETURN wartMax;
  END;
END;

CREATE OR REPLACE TYPE BODY KocuryTyp AS
  MEMBER FUNCTION CalkowityPrzydzial RETURN NUMBER IS
  BEGIN
    RETURN przydzial_myszy+nvl(myszy_ekstra,0);
  END;
  MEMBER FUNCTION Staz RETURN INTEGER IS
  BEGIN
    RETURN sysdate-w_stadku_od;
  END;
END;

CREATE OR REPLACE TYPE BODY WrogowieTyp AS
  MEMBER FUNCTION IleKotow RETURN NUMBER IS
  licz NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO licz
    FROM Wrogowie_Kocurow WK
    WHERE WK.imie_wroga = self.imie_wroga;
    RETURN licz;
  END;
  MEMBER FUNCTION KiedyOstatni RETURN DATE IS
  temp DATE;
  BEGIN
    SELECT MAX(WK.data_incydentu)
    INTO temp
    FROM Wrogowie_Kocurow WK
    WHERE WK.imie_wroga = self.imie_wroga;
    RETURN temp;
  END;
END;

CREATE OR REPLACE TYPE BODY Wrogowie_KocurowTyp AS
  MEMBER FUNCTION DajPowod RETURN VARCHAR2 IS
  BEGIN
    RETURN opis_incydentu;
  END;
END;

CREATE OR REPLACE TYPE BODY ElitaTyp AS
  MEMBER FUNCTION StanKonta RETURN NUMBER IS
  licz NUMBER;
  BEGIN
    SELECT count(*)
    INTO licz
    FROM Konto K
    WHERE K.wlasciciel = self.el_pseudo;
    return licz;
  END;
END;

CREATE OR REPLACE TYPE BODY KontoTyp AS
	MEMBER FUNCTION Dlugosc RETURN NUMBER IS
	BEGIN
		RETURN nvl(data_usun,sysdate)-data_dod;
	END;
END;


------------------------------TESTY---------------------------------------------
SELECT E.el_pseudo, E.StanKonta()
FROM Elita_V E;

SELECT count(*)
from KontoT K, ElitaT E
WHERE K.wlasciciel = REF(E) and E.pseudo = 'TYGRYS';

--18
SELECT K2.imie, K2.w_stadku_od
FROM Kocury_V K1, Kocury_V  K2
WHERE K1.imie = 'JACEK' and k2.staz() > k1.staz();

--21
SELECT max(B.nazwa), COUNT(distinct WK.pseudo)
FROM Bandy_V B, Wrogowie_kocurow WK, Kocury_V K
WHERE WK.pseudo = K.pseudo and K.nr_bandy = REF(B)
GROUP BY B.nr_bandy;

--29
SELECT K.imie, K.CalkowityPrzydzial(), B.nr_bandy, B.Srednia()
FROM Kocury_V K, Bandy_V B
WHERE K.nr_bandy = REF(B) and K.plec = 'M' and K.CalkowityPrzydzial() < B.Srednia();

--37
DECLARE
  cursor crs is
  SELECT K.pseudo, K.CalkowityPrzydzial() przyd
  FROM Kocury_V K
  ORDER BY 2 desc;
  temp1 crs%ROWTYPE;
  counter NUMBER:=0;
BEGIN
  DBMS_OUTPUT.PUT_LINE(RPAD('Nr',5,' ') ||  RPAD('Psuedonim',12,' ') || 'Zjada');
  FOR temp1 in crs LOOP
    counter := counter + 1;
    DBMS_OUTPUT.PUT_LINE(RPAD(counter,5,' ') ||  RPAD(temp1.pseudo,12,' ') || LPAD(temp1.przyd,5,' '));
    EXIT WHEN counter = 5;
  END LOOP;
END;

/*Zad. 48. W zwi�zku z wej�ciem do Unii Europejskiej konieczna sta�a si� 
szczeg�owa ewidencja myszy upolowanych i spo�ywanych. Nale�a�o wi�c 
odnotowywa� zar�wno kota, kt�ry mysz upolowa� (wraz z dat� upolowania) 
jak i kota, kt�ry mysz zjad� (wraz z dat� �wyp�aty�). Dodatkowo istotna 
sta�a si� waga myszy (waga ta musi spe�nia� Unijn� norm� 
(norm� t� prosz� ustali�)). Co najgorsze, jednak, dane nale�a�o uzupe�ni� w 
ty� zaczynaj�c od 1 stycznia 2004. Niestety, jak to czasami bywa, 
nast�pi�o �niewielkie� op�nienie w realizacji programu ewidencjonuj�cego 
upolowane i zjedzone myszy. Dziwnym zbiegiem okoliczno�ci ewidencja ta 
sta�a si� mo�liwa dopiero na dzie� przed terminem oddawania bie��cej listy.

Napisa� blok (bloki), kt�ry zrealizuje ewidencj�, a wi�c:
a.	zmodyfikuje schemat bazy danych o now� relacj� Myszy z atrybutami: 
nr_myszy (klucz g��wny), lowca (klucz obcy), zjadacz (klucz obcy), waga_myszy,
data_zlowienia, data_wydania (zawsze ostatnia �roda miesi�ca),

b.	wype�ni relacj� Myszy sztucznie wygenerowanymi danymi, od 1 stycznia 2004 
pocz�wszy, na dniu poprzednim w stosunku do terminu oddania bie��cej listy 
sko�czywszy. Liczba wpisanych myszy, upolowanych w  konkretnym miesi�cu, ma by�
zgodna z liczb� myszy, kt�re koty otrzyma�y w ramach �wyp�aty� w tym miesi�cu 
(z uwzgl�dnieniem myszy ekstra). W trakcie uzupe�niania danych nale�y przyj�� 
za�o�enie, �e ka�dy kot jest w stanie upolowa� w ci�gu miesi�ca liczb� myszy 
r�wn� liczbie myszy spo�ywanych �rednio w ci�gu miesi�ca przez ka�dego kota 
(�zagospodarowa� ewentualne nadwy�ki zwi�zane z zaokr�gleniami). 
Daty z�owienia myszy maj� by� ustawione �w miar� r�wnomiernie w ci�gu
ca�ego miesi�ca. Dat� wydania ma by� ostatnia �roda ka�dego miesi�ca.
W rozwi�zaniu nale�y wykorzysta� pierwotny dynamiczny SQL 
(tworzenie nowej relacji) oraz pierwotne wi�zanie masowe 
(wype�nianie relacji wygenerowanymi danymi). Od daty bie��cej pocz�wszy maj� 
by� ju� wpisywane rzeczywiste dane dotycz�ce upolowanych myszy. 
Nale�y wi�c przygotowa� procedur�, kt�ra umo�liwi przyj�cie na stan myszy
upolowanych w ci�gu dnia przez konkretnego kota (za�o�y�, �e dane o upolowanych
w ci�gu dnia myszach dost�pne s� w, indywidualnej dla ka�dego kota,
zewn�trznej relacji) oraz procedur�, kt�ra umo�liwi co miesi�czn� wyp�at� 
(myszy maj� by� przydzielane po jednej kolejnym kotom w kolejno�ci zgodnej 
z pozycj� kota w hierarchii stada a� do uzyskania przys�uguj�cego przydzia�u
lub do momentu wyczerpania si� zapas�w).
W obu procedurach nale�y wykorzysta� pierwotne wi�zanie masowe. */

--- tworzenie tabeli
SET SERVEROUTPUT ON

BEGIN
  EXECUTE IMMEDIATE 
    'CREATE TABLE myszy
    ( nr_myszy NUMBER PRIMARY KEY,
      lowca VARCHAR2(15) CONSTRAINT myszy_lowca_fk REFERENCES KOCURY(pseudo),
      zjadacz VARCHAR2(15) CONSTRAINT myszy_zjadacz_fk REFERENCES KOCURY(pseudo),
      waga_myszy NUMBER(2) CONSTRAINT myszy_waga_pomiedzy CHECK (waga_myszy BETWEEN 30 AND 40),
      data_zlowienia DATE NOT NULL,
      data_wydania DATE CONSTRAINT myszy_data_wydania CHECK(data_wydania=(NEXT_DAY(LAST_DAY(data_wydania)-7, ''Wednesday''))),
      CONSTRAINT myszy_wydane_pozniej CHECK (data_zlowienia<=data_wydania)
      )';
EXCEPTION
  WHEN OTHERS 
    THEN
    DBMS_OUTPUT.PUT_LINE(SQLERRM);
END;
/

--- wypelnianie danymi
DECLARE
  TYPE myszy_type IS TABLE OF Myszy%ROWTYPE;
  TYPE pseudo_type IS TABLE OF Kocury.pseudo%TYPE;
  TYPE przydzial_myszy_type IS TABLE OF Kocury.przydzial_myszy%TYPE;
  poczatek DATE;
  koniec DATE:='2015-01-21';
  sroda DATE:='2003-12-31';
  koty pseudo_type;
  przydzialy_myszy przydzial_myszy_type;
  myszki myszy_type:= myszy_type();
  sredni_przydzial_myszy NUMBER(2);
  ost_dodane NUMBER(10):=1;
  ost_wydane NUMBER(10):=1;
  ile_dni NUMBER(2):=0;
BEGIN
  WHILE sroda < koniec 
    LOOP
      poczatek:=sroda + 1;
      sroda:=(NEXT_DAY(LAST_DAY(ADD_MONTHS(sroda,1))-7,'�roda'));
      ile_dni:=sroda-poczatek;
      
      SELECT pseudo, przydzial_myszy + NVL(myszy_ekstra, 0)
      BULK COLLECT INTO koty, przydzialy_myszy
      FROM Kocury
      WHERE w_stadku_od <= poczatek;
      
      SELECT AVG(przydzial_myszy + NVL(myszy_ekstra, 0)) INTO sredni_przydzial_myszy
      FROM KOCURY
      WHERE w_stadku_od <= poczatek;

      FOR i IN 1..koty.COUNT
      LOOP
        FOR j IN 1..CEIL(sredni_przydzial_myszy)
        LOOP
          myszki.EXTEND(1);
          myszki(ost_dodane).nr_myszy:=ost_dodane;
          myszki(ost_dodane).lowca:=koty(i);
          myszki(ost_dodane).waga_myszy:=DBMS_RANDOM.VALUE(30,40); 
          myszki(ost_dodane).data_zlowienia:=poczatek + DBMS_RANDOM.VALUE(0, ile_dni);
          ost_dodane:=ost_dodane + 1;
        END LOOP;
      END LOOP;
      
      FOR i IN 1..koty.COUNT
      LOOP
        FOR j IN 1..przydzialy_myszy(i)
        LOOP
          IF ost_wydane < ost_dodane THEN     
            myszki(ost_wydane).zjadacz:=koty(i);
            myszki(ost_wydane).data_wydania:=sroda;
            ost_wydane:=ost_wydane + 1;
          END IF;
        END LOOP;
      END LOOP;
    END LOOP;

    FORALL i IN 1..myszki.COUNT
      INSERT INTO Myszy 
      VALUES(myszki(i).nr_myszy, myszki(i).lowca, myszki(i).zjadacz, myszki(i).waga_myszy, myszki(i).data_zlowienia, myszki(i).data_wydania);
  END;
/

select count(*) from myszy;

/
--- tworzenie indywidualnych kont kotow
BEGIN
  FOR i IN (SELECT pseudo FROM Kocury)
  LOOP
    EXECUTE IMMEDIATE 
      'CREATE TABLE Myszy_' || i.pseudo || '
      ( nr_myszy NUMBER(3) PRIMARY KEY,
        waga_myszy NUMBER(3) CHECK (waga_myszy BETWEEN 30 and 40),
        data_zlowienia DATE NOT NULL
      )';
  END LOOP;
END;
/

--- przyjecie na stan myszy upolowanych w ciagu dnia przez konkretnego kota
CREATE OR REPLACE PROCEDURE oddawanie_myszy(pse Kocury.pseudo%TYPE) AS
  TYPE myszy_typ IS TABLE OF Myszy%rowtype;
  TYPE myszy_kota_typ IS TABLE OF Myszy_TYGRYS%rowtype;
  myszy_kota myszy_kota_typ := myszy_kota_typ();
  myszki myszy_typ := myszy_typ();
  id_myszy NUMBER(10);
  kot_istnieje NUMBER(2);
  brak_kota EXCEPTION;
BEGIN
  SELECT COUNT(*) INTO kot_istnieje 
  FROM Kocury 
  WHERE pseudo = pse;
  
  IF kot_istnieje = 0 THEN RAISE brak_kota;
  END IF;

  SELECT MAX(nr_myszy) INTO id_myszy
  FROM Myszy;

  EXECUTE IMMEDIATE 
    'SELECT * FROM Myszy_' || pse 
      BULK COLLECT INTO myszy_kota;  -- nowe

  FOR i IN 1..myszy_kota.COUNT
  LOOP
    id_myszy:=id_myszy+1;
    myszki(i).nr_myszy:=id_myszy;
  END LOOP; --

  FORALL i IN 1..myszki.COUNT
    INSERT INTO Myszy VALUES(myszki(i).nr_myszy,myszki(i).lowca,NULL,myszki(i).waga_myszy,myszki(i).data_zlowienia,null);

  EXECUTE IMMEDIATE 
    'DELETE FROM Myszy_' || pse;
EXCEPTION
  WHEN brak_kota 
    THEN DBMS_OUTPUT.PUT_LINE('Nie znaleziono kota');
END;
/


--- co miesieczna wyplata                   
CREATE OR REPLACE PROCEDURE wyplata AS
  TYPE kot_rekord IS RECORD (pseudo Kocury.pseudo%TYPE, myszy Kocury.przydzial_myszy%TYPE);
  TYPE kot_type IS TABLE OF kot_rekord;
  TYPE myszy_type IS TABLE OF myszy%ROWTYPE;
  myszki myszy_type:=myszy_type();
  koty kot_type;
  i NUMBER(10):=1;
BEGIN
  SELECT * 
    BULK COLLECT INTO myszki       --wolne
  FROM Myszy
  WHERE zjadacz IS NULL AND data_wydania IS NULL;

  SELECT pseudo, przydzial_myszy + NVL(myszy_ekstra, 0) myszy
    BULK COLLECT INTO koty
  FROM Kocury
  START WITH szef IS NULL
  CONNECT BY PRIOR pseudo = szef
  ORDER BY LEVEL ASC;

  WHILE i <= myszki.COUNT
  LOOP
    FOR j IN 1..koty.COUNT
    LOOP
      IF koty(j).myszy > 0 THEN
        koty(j).myszy:=koty(j).myszy-1;
        myszki(i).zjadacz:=koty(j).pseudo;
        myszki(i).data_wydania:=NEXT_DAY(LAST_DAY(SYSDATE)-7,'�roda');
        i:=i + 1;
        IF i > myszki.COUNT  THEN EXIT;
        END IF;
      END IF;
    END LOOP;
  END LOOP;

  FORALL k IN 1..myszki.COUNT
    UPDATE myszy 
    SET data_wydania = myszki(k).data_wydania, zjadacz = myszki(k).zjadacz
    WHERE nr_myszy = myszki(k).nr_myszy;
END;
/

-- dodawanie zlapanych myszy na konto kota i przyjecie ich na stan
INSERT ALL 
INTO Myszy_Tygrys VALUES (1, 35, '2015-01-21')
INTO Myszy_Tygrys VALUES (2, 33, '2015-01-21')
INTO Myszy_Tygrys VALUES (3, 40, '2015-01-21')
INTO Myszy_Mala VALUES (4, 31, '2015-01-21')
INTO Myszy_Mala VALUES (5, 35, '2015-01-21')
INTO Myszy_Dama VALUES (6, 38, '2015-01-21')
INTO Myszy_Mala VALUES (7, 38, '2015-01-21')
INTO Myszy_Mala VALUES (8, 38, '2015-01-21')
INTO Myszy_Mala VALUES (9, 33, '2015-01-21')
SELECT * FROM dual;

SELECT * 
FROM Myszy_Tygrys;

EXEC oddawanie_myszy('TYGRYS');
EXEC oddawanie_myszy('MALA');
EXEC oddawanie_myszy('DAMA');

SELECT * 
FROM Myszy_Tygrys;

SELECT * 
FROM
  (SELECT * 
   FROM Myszy 
   ORDER BY nr_myszy DESC)
WHERE ROWNUM <= 3;

DELETE FROM Myszy 
WHERE nr_myszy > ((SELECT MAX(nr_myszy)
                   FROM Myszy)-3);







--- testowanie 
EXEC wyplata();

SELECT * 
FROM Myszy 
ORDER BY nr_myszy DESC;

select count(*)
from myszy
where zjadacz IS NULL;

DELETE FROM myszy;






























drop type BANDYOb force;
drop type ELITAOb force;
drop type FUNKCJEOb force;
drop type KOCURYOb force;
drop type KONTAOb force;
drop type PLEBSOb force;
drop type WROGOWIEOb force;
drop type WROGOWIE_KOCUROWOb force;
drop view KOCURYOID force;
drop view PLEBSOID force;
drop view WROGOWIE_KOCUROWOID force;
drop view WROGOWIEOID force;
drop table BandyT force;
drop table FunkcjeT force;
drop table WrogowieT force;
drop table KocuryT force;
drop table Wrogowie_KocurowT force;
drop table ElitaT force;
drop table PlebsT force;
drop table KontoT force;
drop view bandy_V force;
drop view funkcje_V force;
drop view kocury_V force;
drop view PLEBS_V force;
drop view WROGOWIE_KOCUROW_V force;
drop view WROGOWIE_V force;
drop type BANDYTyp force;
drop type ELITATyp force;
drop type FUNKCJETyp force;
drop type KOCURYTyp force;
drop type KONTATyp force;
drop type PLEBSTYp force;
drop type WROGOWIETyp force;
drop type KontoTyp force;
drop type WROGOWIE_KOCUROWTyp force;
drop table ELITA force;
drop table PLEBS force;


--790
   /* myszki(i).lowca:=pse;
    myszki(i).waga_myszy:=myszy_kota(i).waga_myszy;
    myszki(i).data_zlowienia:=myszy_kota(i).data_zlowienia;*/