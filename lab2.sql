--(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=ORACLE.win.ii.pwr.wroc.pl)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)SERVICE_NAME=orcl.win.ii.pwr.wroc.pl)))
ALTER SESSION SET NLS_Date_Format = 'YYYY-MM-DD';

--Zad. 17. Wy�wietli� pseudonimy, przydzia�y myszy oraz nazwy band dla kot�w 
--operuj�cych na terenie POLE posiadaj�cych przydzia� myszy wi�kszy od 50. 
--Uwzgl�dni� fakt, �e s� w stadzie koty posiadaj�ce prawo do polowa� na ca�ym 
--�obs�ugiwanym� przez stado terenie. Nie stosowa� podzapyta�.

SELECT k.pseudo AS "Poluje w polu", k.przydzial_myszy AS "Przydzial myszy", b.nazwa AS "Nazwa bandy"
FROM Kocury k INNER JOIN Bandy b ON  k.nr_bandy=b.nr_bandy 
WHERE b.teren='POLE' AND k.przydzial_myszy >50 OR szef IS NULL
ORDER BY przydzial_myszy DESC;

--Zad. 18. Wy�wietli� bez stosowania podzapytania imiona i daty przyst�pienia 
--do stada kot�w, kt�re przyst�pi�y do stada przed kotem o imieniu �JACEK�. 
--Wyniki uporz�dkowa� malej�co wg daty przyst�pienia do stadka.

SELECT k2.imie, k2.W_stadku_od AS "Poluje od"
From kocury k INNER JOIN kocury k2 ON k.imie='JACEK' AND k2.W_stadku_OD < k.W_stadku_OD
ORDER BY "Poluje od" DESC;

--Zad. 19. Dla kot�w pe�ni�cych funkcj� KOT i MILUSIA wy�wietli� w kolejno�ci 
--hierarchii imiona wszystkich ich szef�w. Moga byc recznie kolumny!

Select k1.imie, k1.funkcja,k2.imie AS "Szef 1", k3.imie AS "Szef 2", k4.imie AS "Szef 3"
FROM kocury k1 LEFT JOIN Kocury k2 ON k1.szef=k2.pseudo
               LEFT JOIN Kocury k3 ON k2.szef=k3.pseudo
               LEFT JOIN Kocury k4 ON k3.szef=k4.pseudo
WHERE k1.funkcja='KOT' OR k1.funkcja='MILUSIA'
ORDER BY k4.imie,k1.funkcja DESC;

--Zad. 20. Wy�wietli� imiona wszystkich kotek, kt�re uczestniczy�y w incydentach
--po 01.01.2007. Dodatkowo wy�wietli� nazwy band do kt�rych nale�� kotki, 
--imiona ich wrog�w wraz ze stopniem wrogo�ci oraz dat� incydentu.
SELECT k.imie,b.Nazwa AS "Nazwa Bandy",w.imie_wroga AS "Imie wroga", w.stopien_wrogosci AS "Ocena wroga", wk.data_incydentu AS "Data inc."
FROM Kocury k INNER JOIN Wrogowie_kocurow wk ON k.pseudo=wk.pseudo 
              INNER JOIN BANDY b ON k.nr_bandy=b.nr_bandy 
              INNER JOIN WROGOWIE w ON wk.imie_wroga=w.imie_wroga
WHERE k.plec='D' AND Data_incydentu > '2007-01-01'
ORDER BY k.imie, wk.data_incydentu;

--Zad. 21. Okre�li� ile kot�w w ka�dej z band posiada wrog�w.
SELECT b.nazwa AS "Nazwa Bandy",COUNT(DISTINCT wk.pseudo) as "Koty z wrogami"
FROM Bandy b INNER JOIN Kocury k ON b.nr_bandy=k.nr_bandy 
             INNER JOIN Wrogowie_kocurow wk ON k.pseudo=wk.pseudo
GROUP BY b.nazwa;

--Zad. 22. Znale�� koty (wraz z pe�nion� funkcj�), kt�re posiadaj� wi�cej
--ni� jednego wroga.

SELECT f.funkcja, wk.pseudo AS "Pseudonim kota", COUNT(wk.pseudo) AS "Koty z wrogami"
FROM Funkcje f INNER JOIN Kocury k ON f.funkcja=k.funkcja
               INNER JOIN Wrogowie_kocurow wk ON wk.pseudo=k.pseudo
GROUP BY k.pseudo, wk.pseudo, f.funkcja
HAVING  COUNT(wk.pseudo) > 1
ORDER BY f.funkcja;

--Zad. 23. Wy�wietli� imiona kot�w, kt�re dostaj� �mysz�� premi� wraz z ich 
--ca�kowitym rocznym spo�yciem myszy. Dodatkowo je�li ich roczna dawka myszy 
--przekracza 864 wy�wietli� tekst �powyzej 864�, je�li jest r�wna 864 
--tekst �864�, je�li jest mniejsza od 864 tekst �poni�ej 864�. 
--Wyniki uporz�dkowa� malej�co wg rocznej dawki myszy. 
--Do rozwi�zania wykorzysta� operator zbiorowy UNION.

DEFINE dawka_roczna = '(NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0))*12';
SELECT imie AS "Imie", &dawka_roczna AS "Dawka roczna",'ponizej 864' AS "Dawka"
FROM Kocury
WHERE  NVL(myszy_ekstra,0) >0 AND &dawka_roczna < 864
UNION ALL

SELECT imie AS "Imie", &dawka_roczna AS "Dawka roczna",'864' AS "Dawka"
FROM Kocury
WHERE  NVL(myszy_ekstra,0) >0  AND &dawka_roczna = 864
UNION ALL

SELECT imie AS "Imie", &dawka_roczna AS "Dawka roczna",'powyzej 864' AS "Dawka"
FROM Kocury
WHERE  NVL(myszy_ekstra,0) >0  AND &dawka_roczna > 864
ORDER BY "Dawka roczna" DESC;   

/*
DEFINE dawka = '(NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0))*12';
SELECT imie,&dawka AS "Dawka roczna",
( CASE 
      WHEN  &dawka > 864 THEN 'powyzej 864'
      WHEN  &dawka < 864 THEN 'ponizej 864'
      ELSE '864' 
      END
)  AS "Dawka" 
FROM Kocury
WHERE NVL(myszy_ekstra,0) >0
ORDER BY "Dawka roczna" DESC;
*/

--Zad. 24. Znale�� bandy, kt�re nie posiadaj� cz�onk�w. Wy�wietli� ich numery,
--nazwy i tereny operowania. Zadanie rozwi�za� na dwa sposoby: bez podzapyta� 
--i operator�w zbiorowych oraz wykorzystuj�c operatory zbiorowe.

SELECT b.nr_bandy, b.nazwa, b.teren
FROM BANDY b LEFT JOIN KOCURY k ON b.nr_bandy=k.nr_bandy
WHERE k.pseudo IS NULL;
--WHERE b.nr_bandy NOT IN( SELECT nr_bandy  FROM Kocury    );
 
SELECT b.nr_bandy, b.nazwa,b.teren
FROM Bandy b
MINUS
SELECT k.nr_bandy,b.nazwa,b.teren
FROM KOCURY k,Bandy b;
--WHERE b.nr_bandy IN (SELECT nr_bandy FROM BANDY MINUS SELECT nr_bandy FROM KOCURY);


--Zad. 25. Znale�� koty, kt�rych przydzia� myszy jest wy�szy od potrojonego 
--najwy�szego przydzia�u spo�r�d przydzia��w wszystkich MILU� 
--operuj�cych w SADZIE. Nie stosowa� funkcji MAX.

--          ######UWAGA EDIT  
--W rozwi�zaniu zadania 25 z listy nr 2, przez niedopatrzenie, 
--nie uwzgl�dni�em sytuacji, �e koty z bandy nr 1 poluj� na ca�ym terenie 
--a wi�c i w SADZIE. Prawid�owy wi�c wynik to:
--IMIE FUNKCJA PRZYDZIAL MYSZY
----------------- ---------------- ----------------------------
--MRUCZEK SZEFUNIO 103
--            ### KONIEC EDIT

Select imie, funkcja, przydzial_myszy
FROM Kocury 
WHERE przydzial_myszy > 3 *
     ( 
      SELECT pm
      FROM
          (
          SELECT k.przydzial_myszy AS pm
          From Kocury k INNER JOIN Bandy b ON k.nr_bandy=b.nr_bandy
          WHERE k.funkcja='MILUSIA' AND ( b.teren='SAD' OR b.teren='CALOSC')
          ORDER BY k.przydzial_myszy DESC
          )
      WHERE ROWNUM<=1
      )
ORDER BY przydzial_myszy;
       
--Zad. 26. Znale�� funkcje (pomijaj�c SZEFUNIA), z kt�rymi zwi�zany jest
--najwy�szy i najni�szy �redni ca�kowity przydzia� myszy. Nie u�ywa� operator�w
--zbiorowych (UNION, INTERSECT, MINUS).

/*SELECT k.funkcja, MAX(NVL(k.przydzial_myszy,0)) AS "maxi", MIN(NVL(k.przydzial_myszy,0)) AS "mini"
FROM Kocury k
GROUP BY k.funkcja;*/

SELECT k.funkcja,ROUND(AVG(NVL(k.przydzial_myszy,0) + NVL(k.myszy_ekstra,0)),0) AS "Srednio najw. i najm. myszy"
FROM KOCURY k, 
(
 SELECT AVG( NVL(przydzial_myszy,0) + NVL(myszy_ekstra,0) ) AS sr FROM Kocury  WHERE funkcja <> 'SZEFUNIO' GROUP BY funkcja
) t
GROUP BY k.funkcja
HAVING MAX(t.sr) = AVG(NVL(k.przydzial_myszy,0) + NVL(k.myszy_ekstra,0)) OR MIN(t.sr) = AVG(NVL(k.przydzial_myszy,0) + NVL(k.myszy_ekstra,0));

--Zad. 27. Znale�� koty zajmuj�ce pierwszych n miejsc pod wzgl�dem ca�kowitej 
--liczby spo�ywanych myszy (koty o tym samym spo�yciu zajmuj� to samo miejsce!).
--Zadanie rozwi�za� na trzy sposoby:
--a.	wykorzystuj�c podzapytanie skorelowane,
--b.	wykorzystuj�c pseudokolumn� ROWNUM,
--c.	wykorzystuj�c ��czenie relacji Kocury z relacj� Kocury.
--
--Prosz� poda� warto�� dla n: 6


DEFINE spozycie = '(NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0))';
ACCEPT n PROMPT 'Podaj n: ';
SELECT k.pseudo,&spozycie AS "Zjada"
FROM Kocury k
WHERE &n >=
                (
                  SELECT COUNT(DISTINCT &spozycie)
                  FROM Kocury k2
                  WHERE &spozycie >= (NVL(k.przydzial_myszy,0)+NVL(k.myszy_ekstra,0)) 
                  )
 ORDER BY "Zjada" DESC;

--b)
DEFINE spozycie = '(NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0))';
ACCEPT n PROMPT 'Podaj n: ';
SELECT pseudo, &spozycie AS "Zjada"
FROM Kocury
WHERE &spozycie IN
(
  SELECT t."Zjada"
  FROM(
    SELECT &spozycie AS "Zjada"
    FROM KOCURY
    ORDER BY "Zjada" DESC
    ) t
    WHERE ROWNUM <=&n
)
ORDER BY "Zjada" DESC;

--c)
ACCEPT n PROMPT 'Podaj n: ';
SELECT k1.pseudo, AVG(NVL(k1.przydzial_myszy,0)+NVL(k1.myszy_ekstra,0)) AS "Zjada"
FROM Kocury k1, Kocury k2
WHERE (NVL(k1.przydzial_myszy,0)+NVL(k1.myszy_ekstra,0)) <= (NVL(k2.przydzial_myszy,0)+NVL(k2.myszy_ekstra,0))
GROUP BY k1.pseudo
HAVING COUNT( DISTINCT (NVL(k2.przydzial_myszy,0)+NVL(k2.myszy_ekstra,0))) <= &n
ORDER BY "Zjada" DESC;


--Zad. 28. Okre�li� lata, dla kt�rych liczba wst�pie� do stada jest najbli�sza 
--(od g�ry i od do�u) �redniej liczbie wst�pie�  dla wszystkich lat
--(�rednia z warto�ci okre�laj�cych liczb� wst�pie� w poszczeg�lnych latach).
--Nie stosowa� perspektywy.


SELECT rok, wstapien AS "Liczba wstapien"
FROM
(
    SELECT rok, wstapien
    FROM
    (
        SELECT to_char(EXTRACT(YEAR FROM w_stadku_od))AS rok, COUNT(*) AS wstapien
        FROM Kocury
        GROUP BY EXTRACT(YEAR FROM w_stadku_od)
    ) 
    WHERE wstapien =  
    (
        SELECT MIN(wstapien_duzo) 
        FROM
        (
            SELECT wstapien_duzo
            FROM
            (
                SELECT COUNT(*) AS wstapien_duzo 
                FROM Kocury
                GROUP BY EXTRACT(YEAR FROM w_stadku_od)
            )
            WHERE wstapien_duzo >= 
            (
                SELECT AVG(wstapien_srednio)
                FROM
                (
                    SELECT COUNT(*) AS wstapien_srednio
                    FROM Kocury
                    GROUP BY EXTRACT(YEAR FROM w_stadku_od)
                )
            )
        )
    )
    
UNION ALL
SELECT 'Srednia', ROUND(AVG(wstapien),6)
FROM
(
      SELECT to_char(EXTRACT(YEAR FROM w_stadku_od)), COUNT(*) AS wstapien
      FROM Kocury
      GROUP BY EXTRACT(YEAR FROM w_stadku_od)
)

UNION ALL
SELECT rok, wstapien
FROM
(
      SELECT to_char(EXTRACT(YEAR FROM w_stadku_od)) AS rok, COUNT(*) AS wstapien
      FROM Kocury
      GROUP BY EXTRACT(YEAR FROM w_stadku_od)
)
WHERE wstapien = 
(
      SELECT MAX(wstapien_malo) 
      FROM
      (
          SELECT wstapien_malo
          FROM
          (
              SELECT COUNT(*) AS wstapien_malo
              FROM Kocury
              GROUP BY EXTRACT(YEAR FROM w_stadku_od)
          )
          WHERE wstapien_malo <= 
          (
              SELECT AVG(wstapien_srednio)
              FROM
              (
                  SELECT COUNT(*) AS wstapien_srednio
                  FROM Kocury
                  GROUP BY EXTRACT(YEAR FROM w_stadku_od)                      
              )
          )
      )
)
)
ORDER BY wstapien,rok;


--Zad. 29. Dla kocur�w (p�e� m�ska), dla kt�rych ca�kowity przydzia� myszy 
--nie przekracza �redniej w ich bandzie wyznaczy� nast�puj�ce dane: imi�, 
--ca�kowite spo�ycie myszy, numer bandy, �rednie ca�kowite spo�ycie w bandzie. 
--Nie stosowa� perspektywy. Zadanie rozwi�za� na trzy sposoby:
--a.	ze z��czeniem ale bez podzapyta�,
--b.	ze z��czenie i z jedynym podzapytaniem w klauzurze FROM,
--c.	bez z��cze� i z dwoma podzapytaniami: w klauzurach SELECT i WHERE.

--a)
SELECT MAX(k1.imie) AS Imie, (NVL(k1.przydzial_myszy,0)+NVL(k1.myszy_ekstra,0)) AS Zjada, k1.nr_bandy AS "Nr Bandy", AVG( (NVL(k2.przydzial_myszy,0)+NVL(k2.myszy_ekstra,0)) ) AS Srednia
FROM Kocury k1 INNER JOIN Kocury k2 ON k1.nr_bandy = k2.nr_bandy AND k1.plec='M'
GROUP BY k1.nr_bandy, (NVL(k1.przydzial_myszy,0)+NVL(k1.myszy_ekstra,0))
HAVING (NVL(k1.przydzial_myszy,0)+NVL(k1.myszy_ekstra,0)) < AVG( (NVL(k2.przydzial_myszy,0)+NVL(k2.myszy_ekstra,0)) )
ORDER BY k1.nr_bandy DESC;

--b)
DEFINE spozycie = '(NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0))';
SELECT imie, &spozycie AS Zjada, nr_bandy, sb AS "Srednia bandy"
FROM Kocury INNER JOIN 
                (
                  SELECT nr_bandy AS nr, AVG(&spozycie) AS sb
                  FROM Kocury
                  GROUP BY nr_bandy
                )
                ON nr_bandy=nr
WHERE plec='M' AND &spozycie < sb
ORDER BY nr_bandy DESC;

--c)
DEFINE spozycie = '(NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0))';
SELECT imie, &spozycie AS Zjada, nr_bandy, ( SELECT AVG(&spozycie) FROM Kocury k2 WHERE k1.nr_bandy=k2.nr_bandy ) AS "Srednia bandy"
FROM Kocury k1
WHERE plec='M' AND &spozycie < ( SELECT AVG(&spozycie) FROM Kocury k2 WHERE k1.nr_bandy=k2.nr_bandy)
ORDER BY nr_bandy DESC;

--Zad. 30. Wygenerowa� list� kot�w z zaznaczonymi kotami o najwy�szym i 
--o najni�szym sta�u w swoich bandach. Zastosowa� operatory zbiorowe.

SELECT imie, w_stadku_od AS "Wstapil do stadka", ' ' AS " "
FROM Kocury

MINUS
(
  SELECT imie, w_stadku_od, ' '
  FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy=B.nr_bandy
  WHERE w_stadku_od IN
  (
      SELECT MAX(w_stadku_od)
      FROM Kocury
      GROUP BY nr_bandy
  ) 
  OR w_stadku_od IN
  (
      SELECT MIN(w_stadku_od)
      FROM Kocury
      GROUP BY nr_bandy
  )
)

UNION ALL
SELECT imie, w_stadku_od, '<-- Najmlodszy stazem w bandzie '||B.nazwa
FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy=B.nr_bandy
WHERE w_stadku_od IN
(
  SELECT MAX(w_stadku_od)
  FROM Kocury
  GROUP BY nr_bandy
) 

UNION ALL
SELECT imie, w_stadku_od, '<-- Najstarszy stazem w bandzie '||B.nazwa
FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy=B.nr_bandy
WHERE w_stadku_od IN
(
SELECT MIN(w_stadku_od)
FROM Kocury
GROUP BY nr_bandy
)  
ORDER BY imie;

--Zad. 31. Zdefiniowa� perspektyw� wybieraj�c� nast�puj�ce dane: nazw� bandy, 
--�redni, maksymalny i minimalny przydzia� myszy w bandzie, ca�kowit� liczb�
--kot�w w bandzie oraz liczb� kot�w pobieraj�cych w bandzie przydzia�y dodatkowe.
--Pos�uguj�c si� zdefiniowan� perspektyw� wybra� nast�puj�ce dane o kocie, 
--kt�rego pseudonim podawany jest interaktywnie z klawiatury: pseudonim, imi�, 
--funkcja, przydzia� myszy, minimalny i maksymalny przydzia� myszy w jego
--bandzie oraz dat� wst�pienia do stada.

CREATE VIEW zad31
AS
SELECT B.nazwa, AVG(K.przydzial_myszy) AS sre_spoz, MAX(K.przydzial_myszy)  AS max_spoz,  
MIN(K.przydzial_myszy) AS min_spoz, COUNT(*) AS koty, COUNT(K.myszy_ekstra) AS koty_z_dod
FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy=B.nr_bandy
GROUP BY B.nazwa, B.nr_bandy
ORDER BY max_spoz DESC;

SELECT pseudo, imie, funkcja, przydzial_myszy AS Zjada,
      'od '||min_spoz||' do '||max_spoz AS "Granice spozycia", w_stadku_od
FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy = B.nr_bandy
              INNER JOIN ZAD31 ON B.nazwa = ZAD31.nazwa
WHERE pseudo = '&pseudonim';

--Zad. 32. Dla kot�w o trzech najd�u�szym sta�ach w po��czonych bandach 
--CZARNI RYCERZE i �ACIACI MY�LIWI zwi�kszy� przydzia� myszy o 10% minimalnego 
--przydzia�u w ca�ym stadzie lub o 10 w zale�no�ci od tego czy podwy�ka dotyczy 
--kota p�ci �e�skiej czy kota p�ci m�skiej. Przydzia� myszy extra dla kot�w obu
--p�ci zwi�kszy� o 15% �redniego przydzia�u extra w bandzie kota. Wy�wietli� na
--ekranie warto�ci przed i po podwy�ce a nast�pnie wycofa� zmiany.

SELECT pseudo AS "Pseudonim", plec, przydzial_myszy, NVL(myszy_ekstra,0)
FROM Kocury K
WHERE pseudo IN
(
  SELECT pseudo
  FROM
  (
      SELECT pseudo
      FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy=B.nr_bandy
      WHERE B.nazwa='CZARNI RYCERZE'
      ORDER BY w_stadku_od
  )
  WHERE ROWNUM <= 3
  
  UNION ALL
  SELECT pseudo
  FROM
  (
    SELECT pseudo
    FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy=B.nr_bandy
    WHERE B.nazwa='LACIACI MYSLIWI'
    ORDER BY w_stadku_od
  )
  WHERE ROWNUM <= 3
);

UPDATE Kocury K
SET przydzial_myszy = 
CASE 
    WHEN plec = 'D' THEN  przydzial_myszy + (0.1 * (SELECT MIN(przydzial_myszy) FROM Kocury))
    ELSE przydzial_myszy + 10 
END,
    myszy_ekstra = 
    NVL(myszy_ekstra, 0) + 
    (0.15 * 
          (
          SELECT AVG(NVL(myszy_ekstra, 0)) 
          FROM Kocury K1 WHERE K1.nr_bandy = K.nr_bandy
          )
    )
WHERE pseudo IN
(
    SELECT pseudo
    FROM
    (
        SELECT pseudo
        FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy=B.nr_bandy
        WHERE B.nazwa='CZARNI RYCERZE'
        ORDER BY w_stadku_od
    )
    WHERE ROWNUM <= 3
    
    UNION ALL
    SELECT pseudo
    FROM
    (
      SELECT pseudo
      FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy=B.nr_bandy
      WHERE B.nazwa='LACIACI MYSLIWI'
      ORDER BY w_stadku_od
    )
    WHERE ROWNUM <= 3
);
Rollback;

--Zad. 33. Napisa� zapytanie, w ramach kt�rego obliczone zostan� sumy 
--ca�kowitego spo�ycia myszy przez koty sprawuj�ce ka�d� z funkcji z podzia�em 
--na bandy i p�cie kot�w. Podsumowa� przydzia�y dla ka�dej z funkcji. 

DEFINE spozycie = '(NVL(przydzial_myszy,0)+NVL(myszy_ekstra,0))';
SELECT 
  CASE WHEN rownum = 10 THEN nazwa 
  ELSE
    (
      CASE WHEN MOD(rownum,2)<>0 THEN ' ' 
      ELSE nazwa  END
    ) 
  END AS "Nazwa Bandy", plec, liczba AS ile, szefunio, bandzior, lowczy, lapacz, kot, milusia, dzielczy, suma
FROM
(
  SELECT nazwa,
  ( CASE WHEN plec = 'D' THEN 'Kotka' ELSE 'Kocur' END) AS plec,
  to_char(COUNT(*)) AS liczba,
  to_char(nvl(SUM(decode(K.funkcja,'SZEFUNIO',&spozycie)),0)) AS szefunio, 
  to_char(nvl(SUM(decode(K.funkcja,'BANDZIOR',&spozycie)),0)) AS bandzior,
  to_char(nvl(SUM(decode(K.funkcja,'LOWCZY',&spozycie)),0)) AS lowczy,
  to_char(nvl(SUM(decode(K.funkcja,'LAPACZ',&spozycie)),0)) AS lapacz,
  to_char(nvl(SUM(decode(K.funkcja,'KOT',&spozycie)),0)) AS kot,
  to_char(nvl(SUM(decode(K.funkcja,'MILUSIA',&spozycie)),0)) AS milusia,
  to_char(nvl(SUM(decode(K.funkcja,'DZIELCZY',&spozycie)),0)) AS dzielczy,
  to_char(SUM(&spozycie)) AS suma
  FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy = B.nr_bandy
  GROUP BY B.nazwa, K.plec
UNION ALL
(SELECT 'Z---------------','------','----','---------','---------','---------','---------','---------','---------','---------','------'
FROM DUAL
UNION ALL
SELECT 'ZJADA RAZEM', ' ',  ' ',
to_char(nvl(SUM(decode(K.funkcja,'SZEFUNIO',&spozycie)),0)) AS szefunio, 
to_char(nvl(SUM(decode(K.funkcja,'BANDZIOR',&spozycie)),0)) AS bandzior,
to_char(nvl(SUM(decode(K.funkcja,'LOWCZY',&spozycie)),0)) AS lowczy,
to_char(nvl(SUM(decode(K.funkcja,'LAPACZ',&spozycie)),0)) AS lapacz,
to_char(nvl(SUM(decode(K.funkcja,'KOT',&spozycie)),0)) AS kot,
to_char(nvl(SUM(decode(K.funkcja,'MILUSIA',&spozycie)),0)) AS milusia,
to_char(nvl(SUM(decode(K.funkcja,'DZIELCZY',&spozycie)),0)) AS dzielczy,
to_char(SUM(&spozycie)) AS suma
FROM Kocury K INNER JOIN Bandy B ON K.nr_bandy = B.nr_bandy
)
);












/* Kartkowka gr 1
 Wy�wietl pseudo i nr_bandy kot�w plci m�skiej, nie posiadaj�cych wrog�w,
 b�d�cych w grupach, gdzie �rednia przydzialu myszy dla kot�w plci m�skiej 
 jest wi�ksza ni� 55. U�yj grupowania, z��czenia poziomego i 
 podzapytania, ale nie w select ani from. Nie u�ywaj perspektyw, zmiennych 
 i z��czenia pionowego*/  --banda 4
SELECT k.nr_bandy,MAX(k.pseudo),AVG(NVL(k.przydzial_myszy, 0)) AS "Sredni przydzial myszy"
FROM Kocury k LEFT JOIN WROGOWIE_KOCUROW wk ON k.pseudo=wk.pseudo
WHERE Plec='M' AND wk.pseudo IS NULL
GROUP BY k.nr_bandy
HAVING AVG(NVL(k.przydzial_myszy, 0)) > 55;


    SELECT k.pseudo, k.nr_bandy
    FROM Kocury k LEFT JOIN wrogowie_kocurow wk ON k.pseudo = wk.pseudo
    WHERE
            k.plec = 'M'
            AND wk.pseudo IS NULL
   GROUP BY k.nr_bandy
  HAVING AVG(NVL(k.przydzial_myszy, 0)) > 55;
  
  
  Select t.pseudo, k.nr_bandy
  From(
        Select pseudo
        FROM Kocury
        Where Plec='M'
        Minus
        Select pseudo
        From Wrogowie_Kocurow
        ) t INNER JOIN Kocury k ON t.pseudo=k.pseudo
group by t.pseudo,k.nr_bandy
having  AVG(NVL(k.przydzial_myszy, 0)) > 55;

/* Kartkowka 2
Podaj pseudo 5 kot�w o najd�u�szym sta�u w stadzie. 
Koty musz� mie� przydzia� myszy powy�ej po�owy wide�ek (min_myszy i max_myszy)
i pochodzi� z band kt�re maj� wi�cej ni� 4 cz�onk�w. 
U�yj grupowania, podzapytania i z��czenia poziomego (nie w select ani from). 
Nie u�ywaj perspektyw, zmiennych i z��czenia pionowego.
*/

SELECT * -- brak pomyslu na ograniczenie do 5
FROM(
    SELECT k.pseudo
    FROM Kocury k INNER JOIN kocury ko ON k.w_stadku_od<ko.W_STADKU_OD
    Where k.nr_bandy IN
      (
          Select k.nr_bandy
          From kocury k Inner JOIN Bandy b ON k.nr_bandy=b.nr_bandy
          Group by b.nr_bandy
          Having Count(b.nr_bandy) >4
      )
    AND k.przydzial_myszy > (SELECT (min(przydzial_myszy) + max(przydzial_myszy))/2 FROM Kocury)
    GROUP BY k.pseudo
    
    )
WHERE ROWNUM <=5;


	

    SELECT k.pseudo
    FROM kocury k
    JOIN kocury ko ON k.w_stadku_od < ko.w_stadku_od
    WHERE
      4 < 
      (
          SELECT COUNT(*)
          FROM kocury k2
          WHERE k2.nr_bandy = k.nr_bandy
          AND NVL(k.przydzial_myszy, 0) > (SELECT (max_myszy - min_myszy) / 2 FROM funkcje WHERE k.funkcja = funkcja)
    GROUP BY
      k.pseudo
    HAVING
      COUNT(*) <= 5;
      
/*
podaj pseuda  i nr_band kotow plci zenskiej z band w ktorych liczba czlonkin jest mniejsza od liczby ich wrogow
*/
SELECT k.pseudo, k.nr_bandy
FROM kocury k
WHERE k.plec = 'D' AND k.nr_bandy IN 
  (
    SELECT k1.nr_bandy
    FROM kocury k1
    WHERE k1.plec = 'D'
    GROUP BY k1.nr_bandy
    HAVING COUNT(k1.pseudo) < 
      (
        SELECT COUNT(DISTINCT wk.imie_wroga)
        FROM kocury k2 INNER JOIN wrogowie_kocurow wk ON k2.pseudo = wk.pseudo
        WHERE k2.plec = 'D' AND k1.nr_bandy=k2.nr_bandy
      )
  );






